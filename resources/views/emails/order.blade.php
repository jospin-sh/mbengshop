@component('mail::message')
Cher administrateur, une commande vient d'être enregistrée sur votre site mbenshop.com.

Veuillez poursuivre son traitement en vérifiant les coûts en ligne et en procédant à la facturation.
Le plus tôt vous le faites le mieux la confiance client grandira envers la plateforme et le service.

@component('mail::button', ['url' => 'http://mbengshop.com/admin/commande', 'color' => 'success'])
Voir les commandes
@endcomponent

Cordialement,<br>
{{ config('app.name') }}
@endcomponent
