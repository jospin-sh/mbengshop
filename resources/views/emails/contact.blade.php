@component('mail::message')
    Bonjour Cher administrateur, un nouveau message a été laissé sur le site Mbengshop.
    <p>
        Nom: {{$contact->nom}}
    </p>
    <p>
        Prénom: {{$contact->prenom}}
    </p>
    <p>
        Sujet: {{$contact->sujet}}
    </p>
    <div>
        Message:<br/>
        <p>{{$contact->message}}</p>
    </div>

    Cordialement,
    {{ config('app.name') }}
@endcomponent
