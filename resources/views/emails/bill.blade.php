@component('mail::message')
Bonjour M/Mme {{ucfirst($commande->nom)}},
Nous avons reçu votre commande sur notre site mbenshop.com. Merci de recevoir votre facture pro forma dans le présent mail.
<br/>
<div class="bill-box">
    <div class="table-responsive">
        <table class="table">
            <thead class=" text-primary">
            <th>N°</th>
            <th>Nom</th>
            <th>Lien</th>
            <th>Desc.</th>
            <th>Quantité</th>
            <th>P.U(FCFA)</th>
            <th>Poids(kg).</th>
            <th>Total(FCFA)</th>
            </thead>
            <tbody>
            @foreach($commande->articles()->get() as $a=>$article)
                <tr class="article">
                    <th>{{$a+1}}</th>
                    <input type="hidden" name="articles[{{$a}}][id]" value="{{$article->id}}">
                    <td class="text-primary">{{$article->nom}}</td>
                    <td class="text-primary"><a href="{{$article->lien}}">lien</a></td>
                    <td class="text-primary">{{$article->description}}</td>
                    <td class="text-primary">{{$article->quantite}}</td>
                    <td class="text-primary">{{$article->prix}}</td>
                    <td class="text-primary">{{$article->poids}}</td>
                    <td class="text-primary prix_total_article">{{$article->prix * $article->quantite}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <br/>
    <br/>
    <div class="table-responsive">
        <table class="table" border="">
            <thead class=" text-primary">
            <th>Coût Article(facturé par le site)</th>
            <th>Frais de livraison au cameroun</th>
            <th>Commission</th>
            </thead>
            <tbody>
            <tr>
                <td class="text-primary">{{$commande->prix_articles}}</td>
                <td class="text-primary">{{$commande->frais_transport_pays}}</td>
                <td class="text-primary">{{$commande->commission}}</td>
            </tr>
            <tr>
                <td class="text-right" colspan="2">Net à payer</td>
                <td id="total_commande">{{($commande->prix_articles + $commande->frais_transport_pays + $commande->commission)}}
                    FCFA
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="text-align:justify;font-size:14px;">
        <p>
        </p><h4><u><b>Modalités de facturation</b></u></h4>
        <ul>
            <li>
                <b>Coût Articles</b>: C'est le coût du montant total déboursé pour effectuer l'achat sur la
                plateforme e-commerce de vente en ligne; il se compose
                du coût de l'article et des frais de livraison de l'article à l'adresse de Mbengshop en Belgique tel
                que facturé par le site internet et convertit
                en Francs CFA;<br/><br/>
            </li>
            <li>
                <b> Frais de livraison au Cameroun </b>: Frais de transport
                à raison de 6500F par kg (pour tout article de moins de 1 kg les frais sont également de 6500F);
                Bien noté qu'il s'agit ici du poids volumétrique de l'article. Ce poids peut être différent du poids
                en Kg tel que mentionné sur les sites car il prend également en compte le volume occupé par
                l'article;<br/><br/>
            </li>
            <li>
                <b> Commissions </b>: Il s'agit des frais de service de Mbengshop jumelés des frais de douane; Le
                taux est fixé à 10% du montant de votre achat et le minimum est fixé à 2500 FCFA; <br/><br/>
            </li>
        </ul>
        <p></p>
        <p style="text-align:center;background-color:#4c9230;color:#ffffff;">
            <b>
                Si vous êtes d'accord avec cette commande, veuillez répondre à ce mail en disant "Je continue
                l'achat" puis nous vous enverrons un récapitulatif (facture) dans lequel vous aurez toutes les
                modalités de paiement.
            </b>
        </p>
        <div>
            <b><u>NB:</u></b>
            <ol>
                <li style="padding:16px 0;">
                    Votre article vous sera livré au Cameroun dans un délai de
                    <b>
                        15
                        jours maximum
                    </b>
                    après réception de votre colis à notre adresse en Belgique.<i> Par exemple si le site nous livre
                        vos articles 7 Jours après la date du paiement ; vous recevrez vos colis en point de
                        livraison au Cameroun dans un délai de
                        22
                        jours maximum.</i><br/>
                </li>
                <li style="padding:16px 0;">
                    Il est à noter que le poids volumétrique considéré pour vos articles ont été estimés par nos
                    soins. Cependant il peut arriver que nous nous trompions sur le poids facturé et qu'à l'arrivée
                    à notre adresse en Belgique ce dernier soit beaucoup plus élevé. Alors nous serions dans
                    l'obligation de vous demander de compléter la facture pour l'écart constaté. Ceci afin que notre
                    partenaire de fret accepte de transporter votre commande jusqu'au Cameroun.
                </li>
                <li style="padding:16px 0;">
                    Il est également à noter que ce devis ne prend pas en compte les frais de douane que pourraient
                    facturer la douane française à l'arrivée du colis en Belgique. En effet, pour certains vendeurs
                    localisés hors de l'Europe des frais supplémentaires de douane dont nous n'avons aucune
                    connaissance à l'avance pourraient subvenir à l'arrivée du colis en Belgique. Vous serez alors
                    tenus de payer ces frais dès réception de la facture par le transporteur, réception qui pourra
                    survenir à tout moment avant ou après livraison de votre commande.
                </li>
            </ol>
        </div>

    </div>
</div>
Cordialement,<br/>
Service client {{ config('app.name') }}
<div style="min-height:10%;padding-top:20px;text-align:center;background-color:#eee;font-size:14px;">
    <div class="col-md-4">
        <h2 class="footer-heading mb-4">Contactez-nous</h2>
        <ul class="list-unstyled">
            <li><a href="#" class="icon-phone"> (+237) 697502771 / 675972378</a>
            </li>
            <li><a href="#" class="icon-whatsapp"> +32 466 83 14 61 </a>
            </li>
            <li><a href="#" class="icon-contact_mail" style="width: 104%;"> contact@mbengshop.com</a>
            </li>
            <li><a target="_blank" href="https://www.facebook.com/Mbengshop-102046668080345" class="icon-facebook">
                    Facebook</a>
            </li>
            <li><a target="_blank" href="https://www.instagram.com/p/B9UHoWuHRf0/?igshid=1m4i6of0podif"
                   class="icon-instagram"> Instagram</a>
            </li>
        </ul>
    </div>
    <p style="margin-top:30px;">
        Accédez à notre site
    </p>
    <ul style="list-style-type:none;color:#000;padding:0;">
        <li style="margin:10px 0;"><a rel="nofollow" target="_blank" href="https://mbengshop.com"
                                      style="padding-left:30px;line-height:30px;display:inline-block;">mbengshop.com</a>
        </li>
    </ul>
    <p style="margin-top:30px;font-size:14px;">
        Nous espérons vous revoir bientôt.<br/>
        <b>Service client Mbengqhop</b>
    </p>
    <p style="font-size:10px;"><i>Tout paiement vaut confirmation du contrat d'achat. Une fois le paiement effectué,
            les articles ne peuvent être ni échangés, ni remboursés.</i></p>
</div>

@endcomponent
