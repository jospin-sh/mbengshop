@extends('layouts.app')
@section('title')
    Les Commandes
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Liste des commandes</h4>
                        <p class="card-category"> Toutes les commandes passées sur le site s'affichent ici</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                <th>N°</th>
                                <th>Nom</th>
                                <th>Code</th>
                                <th>Etat</th>
                                <th>Date d'enreg.</th>
                                <th>Actions</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($commandes as $k=>$commande)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td>{{$commande->nom}}</td>
                                    <td>{{$commande->code}}</td>
                                    @if($commande->etat == "initial")
                                    <td><span class="btn rd-btn red">{{$commande->etat}}</span></td>
                                    @else
                                        <td><span class="btn rd-btn green">{{$commande->etat}}</span></td>
                                    @endif
                                    <td class="text-primary">{{$commande->created_at}}</td>
                                    <td class="text-primary">
                                        <a href={!! route('commande.show', [$commande->id]) !!} alt="Voir" class="btn btn-primary">
                                        <i class="fa fa-eye"></i> Voir</a>
                                        <a href={!! route('facture', [$commande->id]) !!} alt="Facture" class="btn btn-primary">
                                            <i class="fa fa-eye"></i> Facture</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
