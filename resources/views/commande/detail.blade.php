@extends('layouts.app')
@section('title')
    Commande @isset($commande) {{$commande->code}} @endisset
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Détail</h4>
                        <p class="card-category"> Détail de commande</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                @isset($commande)
                                <tr>
                                    <th>Code</th>
                                    <td colspan="4">{{$commande->code}}</td>
                                </tr>
                                <tr>
                                    <th>Nom</th>
                                    <td colspan="4">{{$commande->nom}}</td>
                                </tr>
                                <tr>
                                    <th>Téléphone</th>
                                    <td colspan="4">{{$commande->telephone}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td colspan="4">{{$commande->email}}</td>
                                </tr>
                                <tr>
                                    <th>Date de création</th>
                                    <td colspan="4">{{$commande->created_at}}</td>
                                </tr>
                                <tr>
                                    <th>Etat</th>
                                    <td colspan="4">{{$commande->etat}}</td>
                                </tr>
                                <tr>
                                    <th>Adresse Livraison</th>
                                    <td colspan="4" class="text-primary">{{$commande->adresse_livraison}}</td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <tr>
                                    <th>Articles commandés</th>
                                    <th class="">Lien</th>
                                    <th class="">Description</th>
                                    <th class="">Quantité</th>
                                    <th class="">Image</th>
                                </tr>
                                @foreach($commande->articles()->get() as $a=>$article)
                                <tr>
                                    <th>Articles {{$a+1}}</th>
                                    <td class="text-primary">{{$article->lien}}</td>
                                    <td class="text-primary">{{$article->description}}</td>
                                    <td class="text-primary">{{$article->quantite}}</td>
                                    @if($article->image)<td class="text-primary">
                                        <img class="img-thumbnail article-img" src="{{asset('storage/'.$article->image)}}" alt=""/></td>@endif
                                </tr>
                                @endforeach
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
