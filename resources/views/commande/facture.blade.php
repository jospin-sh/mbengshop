@extends('layouts.app')
@section('title')
    Facture
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Facture de la commande N° {{$commande->code}}</h4>
                        <p class="card-category"></p>
                    </div>
                    <div class="card-body">
                        <form action="{!! route('facture.send', [$commande->id]) !!}" method="post">
                            @csrf
                            <div class="table-responsive">
                                <h4>
                                    <b>Client:</b> {{$commande->nom}}
                                </h4>
                                <h4>
                                    <b>Telephone / Email:</b> {{$commande->telephone}} / {{$commande->email}}
                                </h4>
                                @if(!empty($commande->articles()->first()->image))
                                <div class="text-primary">
                                    <img class="img-thumbnail article-img" src="{{asset('storage/'.$commande->articles()->first()->image)}}" alt=""/></div>
                                @endif
                                <table class="table" style="width: 100%">
                                    <thead class=" text-primary">
                                    <th>N°</th>
                                    <th>Nom</th>
                                    <th>Lien de l'article</th>
                                    <th>Description</th>
                                    <th>Quantité</th>
                                    <th>P.U(FCFA)</th>
                                    <th>Poids(kg).</th>
                                    <th>Total(FCFA)</th>
                                    </thead>
                                    <tbody>
                                    @foreach($commande->articles()->get() as $a=>$article)
                                        <tr class="article">
                                            <td>{{$a+1}}
                                            <input type="hidden" name="articles[{{$a}}][id]" value="{{$article->id}}">
                                            </td>
                                            <td class="text-primary" style="width: 150px;"><input type="text" name="articles[{{$a}}][nom]" class="form-control nom-cmd" value="{{$article->nom}}" required/></td>
                                            @if(!empty($commande->articles()->first()->image))
                                                <td class="text-primary fixed-td" style="width: 200px; color: black !important;">
                                                    <div class="lien-cmd">
                                                        <textarea name="articles[{{$a}}][lien]" class="form-control" style="width: 200px;
                                                        color: black !important;">{{$article->lien}}</textarea></div>
                                                </td>
                                            @else
                                                <td class="text-primary fixed-td" style="width: 200px; color: black !important;"><div class="lien-cmd">{{$article->lien}}</div></td>
                                            @endif

                                            <td class="text-primary"><input type="text" name="articles[{{$a}}][description]" class="form-control description-cmd" value="{{$article->description}}"/></td>
                                            <td class="text-primary"><input type="number" min="1" name="articles[{{$a}}][quantite]" class="form-control qte-cmd" value="{{$article->quantite}}" required/></td>
                                            <td class="text-primary"><input type="number" min="1" name="articles[{{$a}}][prix]" class="prix_unit form-control" value="{{$article->prix}}" required/></td>
                                            <td class="text-primary"><input type="number" min="0" name="articles[{{$a}}][poids]" class="poids form-control" value="{{$article->poids}}" required/></td>
                                            <td class="text-primary prix_total_article">{{$article->prix * $article->quantite}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-primary text-right" colspan="7">Total(FCFA)</td>
                                        <td class="text-primary total_article" colspan="7">0</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>Coût Article(facturé par le site)</th>
                                    <th>Frais de livraison au cameroun</th>
                                    <th>Commission</th>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-primary"><input type="number" id="prix_articles" name="prix_articles" class="form-control"
                                                                        value="{{$commande->prix_articles}}" required/></td>
                                        <td class="text-primary"><input type="number" id="frais_transport_pays" name="frais_transport_pays" class="form-control"
                                                                        value="{{$commande->frais_transport_pays}}" required/></td>
                                        <td class="text-primary"><input type="number" id="commission" name="commission" class="form-control"
                                                                        value="{{$commande->commission}}" required/></td>
                                    </tr>
                                    <tr class="text-primary">
                                        <td class="text-right" colspan="2">Net à payer</td>
                                        <td id="total_commande">{{($commande->prix_articles + $commande->frais_transport_pays + $commande->commission)}} FCFA</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                @if($commande->etat == \App\Constantes::ETAT_FACTURE_ENVOYE)
                                    Facture envoyée. <button class="btn btn-primary">Renvoyer la facture</button>
                                @else
                                    <button class="btn btn-primary">Envoyer</button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
@endsection
