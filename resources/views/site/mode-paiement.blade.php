@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_5.jpg')}}
@endsection

@section('slide-title')
    Modes de paiement
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span>
        <span>Modes de Paiement</span></p>
@endsection
@section('style')
    <style>
        div.site-blocks-cover{
            background-position-y: 0px
        }
    </style>
@endsection
@section('content')
    <div class="site-section bg-white" id="eu">
        <div class="main_content_area">
            <!-- Main slideshow -->
            <!--/ Main slideshow -->

            <div class="columns-container wide_container">
                <div id="columns" class="container">
                    <div class="row">
                        <div id="center_column" class="center_column col-xs-12 col-sm-12 col-md-12">

                            <div class="rte">
                                <div class="text-left pb-1 border-primary mb-4">
                                    <h2 class="text-primary">Descriptions des moyens de paiement</h2>
                                </div>

                                <p>Chez MbengShop, nous acceptons divers moyens de paiement afin de vous
                                    permettre de choisir celui qui vous correspond. Une fois le paiement effectué, vous
                                    recevrez un sms et un mail de MbengShop confirmant votre paiement. Ainsi vous pouvez
                                    payer par :</p>
                                <div class="row top30">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="cms-box">
                                            <div class="cms-block" id="cash">
                                                <h3>Expèce</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande directement en espèce.
                                                    Pour cela contactez-nous au numéro  +237 697502771 ou +237 675972378 si vous êtes au camerouun
                                                    ou alors +32 466 83 14 61  si vous êtes à l'étranger.</p>
                                            </div>
                                        </div>
                                        <div class="cms-box" id="eumm">
                                            <div class="cms-block">
                                                <h3>Express Union Mobile</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par dépôt
                                                    ou transfert dans notre compte EU Mobile Money au 680 180 068.</p>
                                            </div>
                                        </div>
                                        <div class="cms-box top30" id="om">
                                            <div class="cms-block">
                                                <h3>Orange Money</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par dépôt
                                                    ou par transfert dans notre compte Orange Money au 697 50 27 71.</p>
                                            </div>
                                            <div class="cms-box top30" id="paypal">
                                                <div class="cms-block">
                                                    <h3>VISA</h3>
                                                    <p>Vous pouvez effectuer le paiement de votre commande par transfert en ligne via le service
                                                        VISA.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cms-box top30" id="freecash">
                                            <!--div class="cms-block">
                                                <h3>Versement dans une agence Freecash</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par versement
                                                    dans une agence Free Cash. Ci-dessous la liste des agences :</p>
                                                <ul class="list-3">
                                                    <li>
                                                        <span style="color:#659c26;font-weight:bold;">&gt; Yaoundé</span>
                                                        <p class="top5">FREECASH Nlongkak (Bâtiment école les
                                                            fleurettes)</p>
                                                        <p class="top5">FREECASH Rond Point Express (Face Station
                                                            Oilybia)</p>
                                                        <p class="top5">FREECASH Tsinga (Face Ancienne Mairie
                                                            Tsinga)</p>
                                                    </li>
                                                    <li class="top5"><span style="color:#659c26;font-weight:bold;">&gt; Douala</span>
                                                        <p class="top5">FREECASH Nkololoun (Face SGBC terminus)</p>
                                                    </li>
                                                    <li class="top5"><span style="color:#659c26;font-weight:bold;">&gt; Bafoussam</span>
                                                        <p class="top5">FREECASH Bafoussam (A côté de l'hôtel
                                                            Continental)</p>
                                                    </li>
                                                </ul>
                                            </div-->
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="cms-box" id="ecobank">
                                            <div class="cms-block">
                                                <h3>Virement Bancaire</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par virement /
                                                    dépôt espèces dans notre compte UBA : IBAN = 0010389664.</p>
                                            </div>
                                        </div>
                                        <div class="cms-box top30" id="mtn">
                                            <div class="cms-block">
                                                <h3>MTN Mobile Money</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par dépôt ou par
                                                    transfert dans notre compte MTN Mobile Money au 675 97 23 78.</p>
                                            </div>
                                        </div>
                                        <div class="cms-box top30" id="paypal">
                                            <div class="cms-block">
                                                <h3>PAYPAL</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par dépôt ou par
                                                    transfert dans notre compte PAYPAL d'adresse mail mbengshop@gmail.com.</p>
                                            </div>
                                        </div>
                                        <div class="cms-box top30" id="paypal">
                                            <div class="cms-block">
                                                <h3>MASTERCARD</h3>
                                                <p>Vous pouvez effectuer le paiement de votre commande par transfert en ligne via le service
                                                    MASTERCARD.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div><!-- #center_column -->
                    </div><!-- .row -->
                </div><!-- #columns -->
            </div><!-- .columns-container -->

            <div class="main_content_area_footer">
                <div class="wide_container"></div>
            </div>
        </div>
    </div>
@endsection