@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_3.jpg')}}
@endsection

@section('slide-title')
    Inscription
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="index.html">Accueil</a> <span class="mx-2">&gt;</span> <span>Inscription</span></p>
@endsection

@section('content')
    <div class="site-section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mb-5">
                    <form action="#" class="p-5 bg-white">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="nom">Nom</label>
                                <input type="text" id="nom" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="prenom">Prénom</label>
                                <input type="text" id="prenom" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="telephone">Téléphone</label>
                                <input type="text" id="telephone" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="email" id="email" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="password">Mot de passe</label>
                                <input type="password" id="password" class="form-control">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Se connecter" class="btn btn-primary py-2 px-4 text-white">
                            </div>
                        </div>


                    </form>
                </div>
                <div class="col-md-5">

                    <div class="p-4 mb-3 bg-white">
                        <p class="mb-0 font-weight-bold">Addresse</p>
                        <p class="mb-4">Akwa Douala-bar, Cameroun</p>

                        <p class="mb-0 font-weight-bold">Téléphone</p>
                        <p class="mb-4"><a href="#">+237 697502771/+237 675972378</a></p>

                        <p class="mb-0 font-weight-bold">Email</p>
                        <p class="mb-0"><a href="#">contact@mbengshop.com</a></p>

                    </div>

                    <div class="p-4 mb-3 bg-white">
                        <h3 class="h5 text-black mb-3">Plus d'information</h3>
                        <p>Contactez-nous pour en savoir plus sur nos services d'achat et de transfert de colis. </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection