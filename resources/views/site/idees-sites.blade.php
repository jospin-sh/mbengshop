@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_3.jpg')}}
@endsection

@section('slide-title')
    Contact
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span> <span>Contact</span></p>
@endsection

@section('content')
    <div class="site-section bg-light"  id="scroll-point">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-mode-tab" data-toggle="tab" href="#nav-mode" role="tab"
                               aria-controls="nav-mode" aria-selected="true">Mode et vêtements</a>
                            <a class="nav-item nav-link" id="nav-parfum-tab" data-toggle="tab" href="#nav-parfum" role="tab"
                               aria-controls="nav-parfum" aria-selected="false">Parfum et bien-être</a>
                            <a class="nav-item nav-link" id="nav-occasion-tab" data-toggle="tab" href="#nav-occasion" role="tab"
                               aria-controls="nav-occasion" aria-selected="false">Articles d’occasion</a>
                            <a class="nav-item nav-link" id="nav-informatique-tab" data-toggle="tab" href="#nav-informatique" role="tab"
                               aria-controls="nav-informatique" aria-selected="false">Informatique et électronique</a>
                            <a class="nav-item nav-link" id="nav-electronique-tab" data-toggle="tab" href="#nav-electronique" role="tab"
                               aria-controls="nav-electronique" aria-selected="false">Composants électroniques</a>
                            <a class="nav-item nav-link" id="nav-automobile-tab" data-toggle="tab" href="#nav-automobile" role="tab"
                               aria-controls="nav-automobile" aria-selected="false">Automobile</a>
                        </div>
                    </nav>
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent" style="padding-left: 16px!important; padding-right: 16px!important">
                        <div class="tab-pane fade show active" id="nav-mode" role="tabpanel" aria-labelledby="nav-mode-tab" aria-selected="true">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.zara.com/be/fr/">
                                        <div class="h-entry">
                                            <img src="images/zara.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">ZARA</h2>
                                            <p class="black">ZARA - MODE FEMMES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.asos.com/women/outlet/cat/?cid=27391">
                                        <div class="h-entry">
                                            <img src="images/asos.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">ASOS</h2>
                                            <p class="black">ASOS - MODE FEMMES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank"
                                       href="https://fr.zalando.be/accueil-homme/?&wmc=SEM321_BR_GO._4566764697_1069983304_52041134346.&opc=2211&gclid=Cj0KCQiAtOjyBRC0ARIsAIpJyGNlxSrEgOUV77sZ3xZD7lHLE6AlzbjZXUm4hRZIZjqv-ZIrviNk4OMaAtc5EALw_wcB&gclsrc=aw.ds">
                                        <div class="h-entry">
                                            <img src="images/zalando.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">
                                                ZALANDO</h2>
                                            <p class="black">ZALANDO - MODE HOMMES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.c-and-a.com/fr/fr/shop/promos">
                                        <div class="h-entry">
                                            <img src="images/c-and-a.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">C-AND-A</h2>
                                            <p class="black">C-AND-A - MODE HOMMES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.bonprix-wa.be">
                                        <div class="h-entry">
                                            <img src="images/bonprix.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">Bon Prix</h2>
                                            <p class="black">Bon Prix - MODE FEMMES</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-parfum" role="tabpanel" aria-labelledby="nav-parfum-tab">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.planetparfum.com">
                                        <div class="h-entry">
                                            <img src="images/planet_parfum.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">Planet Parfum</h2>
                                            <p class="black">Planet Parfum - Parfum</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.douglas.nl">
                                        <div class="h-entry">
                                            <img src="images/douglas.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">DOUGLAS</h2>
                                            <p class="black">DOUGLAS - PARFUM</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank"
                                       href="https://www.notino.be">
                                        <div class="h-entry">
                                            <img src="images/notino.png" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">
                                                NOTINO</h2>
                                            <p class="black">NOTINO - Parfum</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-occasion" role="tabpanel" aria-labelledby="nav-occasion-tab">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.cashconverters.be">
                                        <div class="h-entry">
                                            <img src="images/cashconverter.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">CASHCONVERTERS</h2>
                                            <p class="black">CASHCONVERTERS - ARTICLES D'OCCASION</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.ebay.fr">
                                        <div class="h-entry">
                                            <img src="images/ebay.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">EBAY</h2>
                                            <p class="black">EBAY - ARTICLES D'OCCASION</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-informatique" role="tabpanel" aria-labelledby="nav-informatique-tab">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.krefel.be/fr">
                                        <div class="h-entry">
                                            <img src="images/krefel.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">KREFEL</h2>
                                            <p class="black">KREFEL - INFORMATIQUE</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.electrodepot.be/">
                                        <div class="h-entry">
                                            <img src="images/electrodepot.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">ELECTRODEPOT</h2>
                                            <p class="black">ELECTRODEPOT - ELECTRONIQUE</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank"
                                       href="https://www.mediamarkt.be/fr">
                                        <div class="h-entry">
                                            <img src="images/mediamarkt.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">
                                                MEDIAMARKT</h2>
                                            <p class="black">MEDIAMARKT - INFORMATIQUE</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.cdiscount.com">
                                        <div class="h-entry">
                                            <img src="images/cdiscount.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">CDISCOUNT</h2>
                                            <p class="black">CDISCOUNT - ELECTRONIQUE</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.amazon.fr">
                                        <div class="h-entry">
                                            <img src="images/amazon.png" alt="Image" class="img-fluid" style="/*border: 1px solid grey*/">
                                            <h2 class="font-size-regular">AMAZON</h2>
                                            <p class="black">AMAZON - DIVERS</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-electronique" role="tabpanel" aria-labelledby="nav-electronique-tab">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.bol.com">
                                        <div class="h-entry">
                                            <img src="images/bol.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">BOL</h2>
                                            <p class="black">BOL - COMPOSANTS ELECTRONIQUES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.mouser.be">
                                        <div class="h-entry">
                                            <img src="images/mouser.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">MOUSER</h2>
                                            <p class="black">MOUSER - COMPOSANTS ELECTRONIQUES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank"
                                       href="https://www.mantec.be/fr">
                                        <div class="h-entry">
                                            <img src="images/mantec.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">MANTEC</h2>
                                            <p class="black">MANTEC - COMPOSANTS ELECTRONIQUES</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-automobile" role="tabpanel" aria-labelledby="nav-automobile-tab">
                            <div class="row">
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.mister-auto.be/fr">
                                        <div class="h-entry">
                                            <img src="images/misterauto.jpg" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">MISTER AUTO</h2>
                                            <p class="black">MISTER AUTO - AUTOMOBILE ET PIECES</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                                    <a target="blank" href="https://www.autoscout24.be/fr">
                                        <div class="h-entry">
                                            <img src="images/autoscout24.png" alt="Image" class="img-fluid">
                                            <h2 class="font-size-regular">AUTOSCOUT24</h2>
                                            <p class="black">AUTOSCOUT24 - AUTOMOBILE ET PIECES</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection