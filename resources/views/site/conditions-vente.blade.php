@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_5.jpg')}}
@endsection

@section('slide-title')
    Modes de paiement
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span>
        <span>Modes de Paiement</span></p>
@endsection
@section('style')
    <style>
        div.site-blocks-cover {
            background-position-y: 0px
        }
    </style>
@endsection
@section('content')
    <div class="site-section bg-white" id="eu">
        <div class="main_content_area">
            <!-- Main slideshow -->
            <!--/ Main slideshow -->

            <div class="columns-container wide_container">
                <div id="columns" class="container">
                    <div class="row">
                        <div class="row">
                            <div class="large-12 columns">
                                <h2 align="center">Conditions générales de vente</h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>mbengshop</b></h2>
                                <p>mbengshop, votre facilitateur d’achats à l’étranger, est un établissement de droit
                                    camerounais immatriculé au registre de commerce sous le XXXXXXXXXXX dont le siège
                                    est situé sis à total école de police, BP : XXXX Yaoundé.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Conclusion du contrat</b></h2>
                                <p>Toute commande d’articles implique l'adhésion sans réserve aux présentes conditions
                                    générales de vente, complétées ou aménagées par nos conditions particulières, qui
                                    dérogeront à ces dispositions générales.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Les prestations</b></h2>
                                <p>Les prestations de mbengshop comprennent deux types de services. Le premier service
                                    est celui d’achat et de livraison au Cameroun qui s’effectue soit selon le modèle
                                    classique soit celui express. Le second service est celui de la livraison exclusive
                                    après achat des articles par les propres soins du client et livraison à notre
                                    entrepôt en France.</p>
                                <p>Concernant le service livraison exclusif des colis, appelé mbengshop delivery, les
                                    conditions suivantes sont applicables:</p>
                                <ol style="margin-left:2em;">
                                    <li>Le poids volumétrique de l’article est pris en compte; c’est–à-dire le poids
                                        prenant en compte le volume occupé par le colis. Il faut donc tenir compte des
                                        dimensions du carton dans lequel sera emballé le colis.
                                    </li>
                                    <li>Les colis ne sont pas reconditionnables ; les colis sont livrés tels que reçus
                                        par l’expéditeur.
                                    </li>
                                    <li>Il est impossible de renvoyer les colis à l’expéditeur dès lors qu’ils sont été
                                        livrés dans les locaux de mbengshop.
                                    </li>
                                    <li>Après réception du colis, mbengshop mesure le poids volumétrique et établit une
                                        facture pro-forma que le client devra régler avant toute expédition.
                                    </li>
                                </ol>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Les produits exclus du champ de prestation de mbengshop</b></h2>
                                <p>Ne relèvent pas du domaine des services de mbengshop la livraison des produits jugés
                                    illicites dont la substance peut se traduire comme suit :</p>
                                <ul style="margin-left:2em;">
                                    <li>Matières biologiques périssables, infectieuses ou non infectieuses. Organes et
                                        dépouilles humains ;
                                    </li>
                                    <li>Stupéfiants et matières psychotropes ;</li>
                                    <li>Objets obscènes ou immoraux ;</li>
                                    <li>Produits de contrefaçon ;</li>
                                    <li>Animaux vivants (sauf conditions spécifiques) ;</li>
                                    <li>Animaux morts ;</li>
                                    <li>Matières explosives (y compris les munitions inertes ou factices), inflammables
                                        (alcools, parfums) radioactives ou corrosives ;
                                    </li>
                                    <li>Matières dangereuses telles que le gaz comprimé ou tout objet contenant du gaz
                                        ou de l’air comprimé ou liquide (produits aérosols, canots de sauvetage,
                                        extincteurs, etc...) ;
                                    </li>
                                    <li>Produits à caractère militaire ou pouvant mettre en péril l’intégrité du
                                        territoire national (drones, armes, etc….) ;
                                    </li>
                                    <li>Objets qui par leur nature ou leur emballage, peuvent présenter un danger pour
                                        les agents, salir ou détériorer les affaires personnelles du voyageur ou des
                                        autres voyageurs ou l’équipement utilisé par le voyageur ;
                                    </li>
                                    <li>Biens culturels ayant un intérêt majeur pour le patrimoine national;</li>
                                    <li>Envois dont le caractère sensible est soumis à des formalités particulières :
                                        armes, médicaments, tabacs, amiante, marchandises stratégiques utilisables à des
                                        fins civiles ou militaires ou produits issus d’espèces animales ou végétales
                                        protégées par la Convention de Washington.
                                    </li>
                                    <li>De plus, mbengshop se réserve le droit de refuser tout envoi qui serait jugé
                                        impossible à transporter en toute sécurité et conformément à la Loi.
                                    </li>
                                </ul>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Modalités de facturation</b></h2>
                                <ol style="margin-left:2em;">
                                    <li>
                                        <strong>Service achat + livraison classique</strong>
                                        <p>mbengshop facturera au client les frais détaillés comme suit :</p>
                                        <ul style="margin-left:2em;">
                                            <li><strong> Coût Articles: </strong> Il s'agit du montant total déboursé
                                                pour effectuer l'achat sur le site internet de vente en ligne; il est
                                                donc composé du coût de l'article et des frais de livraison de l'article
                                                à l'adresse de mbengshop en France tel que facturé par le site internet
                                                et convertit en Francs CFA;
                                            </li>
                                            <li><strong> Frais de livraison au Cameroun: </strong> Frais de transport à
                                                raison de 10000F par kg (pour tout article de moins de 1 kg les frais
                                                sont également de 10000F) ; Bien noté qu'il s'agit ici du poids
                                                volumétrique de l'article. Ce poids peut être différent du poids en Kg
                                                tel que mentionné sur les sites car il prend également en compte le
                                                volume occupé par l'article.
                                            </li>
                                            <li><strong> Commission mbengshop: </strong> Frais de service de mbengshop à
                                                raison de 25% du cout de l'article;
                                            </li>
                                            <li><strong> Frais de livraison point relais: </strong> Pour tout colis
                                                livré à Yaoundé ces frais sont de 0 CFA mais pour toute livraison dans
                                                une autre ville du Cameroun, les frais varient en fonction de la ville
                                                et du volume du colis.
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <strong>Service achat + livraison express</strong>
                                        <p>Les modalités de facturation sont similaires au service classique mais compte
                                            tenu de la célérité les frais de livraison au Cameroun sont estimés à 15
                                            000F par kg.</p>
                                    </li>
                                    <li>
                                        <strong>Service livraison classique</strong>
                                        <p>Ici, sont facturés au client uniquement les frais de transport de sa
                                            marchandise jusqu’au Cameroun. Frais qui compte tenu du suivi et de la
                                            personnalisation requise s’élèvent à 15000 FCFA par Kg.</p>
                                    </li>
                                    <li>
                                        <strong>Service livraison express</strong>
                                        <p>Pour le service de livraison exclusif, la version express (livraison garantie
                                            en moins de 7 jours au Cameroun) sera facturée à 20000 FCFA par Kg
                                            transporté.</p>
                                    </li>
                                </ol>

                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Points de livraison</b></h2>
                                <p>La livraison est effectuée par l’entremise de notre entreprise partenaire AFRIRELAY
                                    qui dispose d’une multitude de points relais répartie sur l’étendue du territoire
                                    Camerounais.</p>
                                <p>Pour tout besoin d’information sur la liste des points relais, bien vouloir svp vous
                                    rendre sur le site <a href="http://www.afrirelay.com" target="_blank">www.afrirelay.com</a>.
                                </p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Garantie et délais de réclamation</b></h2>
                                <p>mbengshop dispose d’une équipe en charge de la vérification de la véracité,
                                    conformité et authenticité des articles dès leur réception à l’adresse en France. La
                                    vérification des marchandises par l'acheteur doit être effectuée au moment de leur
                                    retrait dans les points de livraison. <br> En cas d'avarie ou de manquant, de
                                    réclamations sur les vices apparents ou sur la non-conformité du produit livré, le
                                    client émettra des réserves claires et précises qu'il notifiera dans un délai de
                                    48h, suivant la date de retrait par écrit auprès du service mbengshop.
                                    L’inobservation de ces délais de réclamation entrainera ipso facto une exonération
                                    de responsabilité de mbengshop. Il appartiendra aux clients de fournir tout
                                    justificatif quant à la réalité des anomalies constatées. <br> mbengshop apportera
                                    le plus grand soin à l'exécution de la commande et à la qualité des produits. En cas
                                    de défectuosité reconnue par mbengshop, l'obligation de ce dernier sera limitée au
                                    remplacement ou au remboursement des quantités défectueuses, sans autre Indemnité.
                                </p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Exonérations de responsabilité de mbengshop</b></h2>
                                <ol style="margin-left:2em;">
                                    <li>mbengshop ne pourra être tenu responsable en cas d’indisponibilité de produits
                                        spécifiés dans une Commande ;
                                    </li>
                                    <li>mbengshop ne pourra être tenu responsable de la bonne ou de la mauvaise qualité
                                        des produits commandés par les clients. De plus, il ne lui incombe pas la
                                        responsabilité des articles dans la mesure où le client est supposé avoir
                                        préalablement vérifié le lien de l’article sur sa facture ;
                                    </li>
                                    <li>mbengshop ne saurait être responsable du non-respect des délais de livraison par
                                        le site marchand ;
                                    </li>
                                    <li>Pour les cas de livraison uniquement, mbengshop ne saurait être responsable de
                                        l’état des colis livrés ; il incombe au client de s’assurer du bon
                                        conditionnement des colis avant leur expédition à notre adresse en France. De
                                        plus mbengshop ne pourra être tenu responsable dans le cas où un Destinataire
                                        aurait recours à une utilisation frauduleuse de moyens de paiements pour
                                        effectuer une Commande
                                    </li>
                                </ol>

                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Récapitulatif de commande</b></h2>
                                <p>Pour plus de sécurité et de précision, avant le paiement de toute commande, il est
                                    établit un récapitulatif de commande au client afin que celui-ci puisse vérifier le
                                    lien et les données disponibles dans sa facture avant d’effectuer tout paiement. En
                                    conséquence de quoi, mbengshop ne sera pas responsable du mauvais choix d’un article
                                    par le client ou de la qualité de ce dernier.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Paiement</b></h2>
                                <p>
                                    Le paiement est préalable à toute livraison. Sauf conditions particulières, le
                                    paiement est possible à compter de la date d’émission d’une pro-forma. Pour le
                                    paiement des factures, une collaboration fiable et efficace a été mise en place :
                                    Express Union, Global Numerica, virement bancaire, Orange Money et MTN Mobile Money
                                    sont les moyens de paiement possible. <br> Le paiement effectué dans une agence
                                    Global Numerica, Express union, est suivi de la délivrance d’un reçu « mbengshop »
                                    qui fera office de garantie. Si, a contrario, c'est via Orange Money ou MTN Money ou
                                    par virement bancaire, notre mail de confirmation de paiement sera considéré de
                                    plein droit comme une garantie certaine. <br> Aucun paiement à échéance multiple
                                    n’est accepté. La commande demeure suspendue jusqu’au paiement intégral. </p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Potentiels frais supplémentaires après paiement</b></h2>
                                <p>
                                    Après le paiement de sa facture chez mbengshop un client peut être emmené à payer
                                    des frais supplémentaires dus à des éléments survenus après la cotation. </p>
                                <ol>
                                    <li>
                                        <strong>Poids mal estimé</strong><br>
                                        Dans le cas des proformas achat et livraison, le poids volumétrique considéré
                                        pour vos articles a été estimé par nos soins. Cependant il peut arriver que nous
                                        nous trompions sur le poids facturé et qu'à l'arrivée à notre adresse en France
                                        ce dernier soit beaucoup plus élevé. Alors nous serions dans l'obligation de
                                        vous demander de compléter la facture pour l'écart constaté. Ceci afin que notre
                                        partenaire de fret accepte de transporter votre commande jusqu'au Cameroun.
                                    </li>
                                    <li>
                                        <strong>Frais de douane facturés à l'arrivée du colis en France</strong><br>
                                        Les cotations effectuées ne prennent pas en compte les frais de douane que
                                        pourraient facturer la douane française à l'arrivée du colis en France. En
                                        effet, pour certains vendeurs localisés hors de l'Europe des frais
                                        supplémentaires de douane dont nous n'avons aucune connaissance à l'avance
                                        pourraient subvenir à l'arrivée du colis en France. Vous serez alors tenus de
                                        payer ces frais dès réception de la facture par le transporteur, réception qui
                                        pourra survenir à tout moment avant ou après livraison de votre commande.
                                    </li>
                                </ol>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Conditions de retour</b></h2>
                                <p>Toute marchandise achetée via mbengshop ne peut être retournée ou échangée sauf dans
                                    le cas où l’article ne correspond pas à celui commandée. Dans ce cas mbengshop
                                    engagera toutes les démarches auprès du site marchand pour que le client entre en
                                    possession de l’article commandé ou soit remboursé.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Réserve de propriété</b></h2>
                                <p>Les articles ou produits achetés restent la propriété de mbengshop jusqu'à leur
                                    livraison dans les points relais. Toutefois les risques afférents aux articles
                                    seront transférés au client, dès le retrait des dits produits des points relais.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>

                        <div class="row">
                            <div class="large-12 columns">
                                <h2><b>Droit applicable, for jurique et attribution de juridiction</b></h2>
                                <p>Tout litige relatif à l’application des présentes et à la mise en œuvre de nos
                                    services sera de la compétence non-exclusive des juridictions du pays d’origine de
                                    l’article. Les différends y afférant seront tranchés conformément aux dispositions
                                    du droit international privé auxquelles les parties seront assujetties.</p>
                                <hr>
                                <hr class="ef-blank hide-for-print">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection