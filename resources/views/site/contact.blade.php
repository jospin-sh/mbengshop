@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_3.jpg')}}
@endsection

@section('slide-title')
    Contact
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span> <span>Contact</span></p>
@endsection

@section('content')
    <div class="site-section bg-light" id="scroll-point">
        <div class="container">
            @component('helpers.alert')
                .
            @endcomponent
            <div class="row">
                <div class="card col-md-7 mb-5">
                    <div class="card-body">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h2 class="text-primary">Contactez-nous</h2>
                        </div>
                        <form action="{!! route('contactus') !!}" method="post" class="p-5 bg-white">
                        @csrf
                        <div class="row form-group">
                            <div class="col-md-6 mb-3 mb-md-0">
                                <label class="text-black" for="fname">Nom</label>
                                <input type="text" id="fname" name="nom" class="form-control" required autofocus>
                            </div>
                            <div class="col-md-6">
                                <label class="text-black" for="lname">Prénom</label>
                                <input type="text" id="lname" name="prenom" class="form-control" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="subject">Sujet</label>
                                <input type="subject" id="subject" name="sujet" class="form-control" required>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="message">Message</label>
                                <textarea name="message" id="message" cols="30" rows="7" class="form-control"
                                          placeholder="Ecrivez votre message ici." required></textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Envoyez" class="btn btn-primary py-2 px-4 text-white" required>
                            </div>
                        </div>

                    </form>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card box-sh" style="min-height: 804px;">
                        <div class="card-body">
                            <div class="p-4 mb-3 bg-white">
                                <p class="mb-0 font-weight-bold">Adresse</p>
                                <p class="mb-4">Akwa Douala-bar, Cameroun</p>

                                <p class="mb-0 font-weight-bold">Téléphone</p>
                                <p class="mb-4"><a href="#" style="font-family: 'icomoon' !important;">+237 697502771 / +237 675972378</a></p>
                                <p class="mb-4"><a href="#" class="icon-whatsapp"> +32 466 83 14 61 </a></p>

                                <p class="mb-0 font-weight-bold">Email</p>
                                <p class="mb-0"><a href="#">contact@mbengshop.com</a></p>

                            </div>
                            <div class="p-4 mb-3 bg-white">
                        <h3 class="h5 text-black mb-3">Plus d'information</h3>
                        <p>Contactez-nous pour en savoir plus sur nos services d'achat et de transfert de colis. </p>
                    </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection