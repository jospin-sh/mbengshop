@extends('layouts.app-site')

@section('content')
    <div class="site-section bg-light">
        <div class="container">
            <div class="row mb-5">
                @component('helpers.alert')
                    .
                @endcomponent
                <div class="text-left pb-1 border-primary mb-4">
                    <h2 class="text-primary">Connectez-vous ou créez votre compte.</h2>
                </div>
                <div class="row">
                    <div class="col-md-6 mb-5">
                        <form class="p-5 bg-white box-sh" method="POST" action="{{ route('login') }}">
                            @csrf
                            <h4>Déjà inscrit? Connectez-vous</h4>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="email">Email</label>
                                    <input type="email" name="email" id="email"
                                           class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="password">Mot de passe</label>
                                    <input type="password" name="password" id="password"
                                           class="form-control @error('email') is-invalid @enderror">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="submit" value="Se connecter"
                                           class="btn btn-primary py-2 px-4 text-white">
                                </div>
                            </div>


                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="{!! route('site.register') !!}" method="POST" class="p-5 bg-white box-sh">
                            @csrf
                            <h4>Nouveau client ? Créez un compte</h4>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="nom">Nom*</label>
                                    <input type="text" name="nom" id="nom" class="form-control @error('nom') is-invalid @enderror" required>
                                    @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="prenom">Prénom*</label>
                                    <input name="prenom" type="text" id="prenom"
                                           class="form-control @error('prenom') is-invalid @enderror" required>
                                    @error('prenom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="telephone">Téléphone*</label>
                                    <input name="telephone" type="text" id="telephone"
                                           class="form-control @error('telephone') is-invalid @enderror" required>
                                    @error('telephone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="email">Email*</label>
                                    <input type="email" name="email" id="email"
                                           class="form-control @error('email') is-invalid @enderror" required>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label class="text-black" for="password">Mot de passe*</label>
                                    <input type="password" name="password" id="password"
                                           class="form-control @error('password') is-invalid @enderror" required>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="submit" value="S'inscrire"
                                           class="btn btn-primary py-2 px-4 text-white">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection