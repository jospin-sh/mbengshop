@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_2.jpg')}}
@endsection

@section('slide-title')
    Services
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Home</a> <span class="mx-2">&gt;</span> <span>Services</span></p>
@endsection

@section('content')
    <div class="site-section" id="scroll-point">
        <div class="container">
            <div class="text-left pb-1 border-primary mb-4">
                <h2 class="text-primary">Nos services</h2>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_3.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Achat en ligne</h3>
                            <p class="px-5">Vous désirez faire des achats en ligne mais n'avez pas de cartes? Envoyez-nous les liens des articles et nous faisons les achats pour vous.</p>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_6.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Espace de rangement</h3>
                            <p class="px-5">Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté</p>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_2.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Transport aérien</h3>
                            <p class="px-5">Vos souhaitez envoyer un colis de moyen ou grand volume au cameroun? Nous sommes experts dans le transfert de marchandise par cargo.</p>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_4.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Transport par Cargo</h3>
                            <p class="px-5">Vous souhaitez un envoyer un colis de moyen ou grand volume au cameroun? Nous sommes experts dans le transfert de marchandise par cargo.</p>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_5.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Entreposage</h3>
                            <p class="px-5">Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté.</p>
                        </div>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 mb-4">
                    <a href="#" class="unit-1 text-center">
                        <img src="images/img_3.jpg" alt="Image" class="img-fluid">
                        <div class="unit-1-text">
                            <h3 class="unit-1-heading">Transfert de colis</h3>
                            <p class="px-5">Nous transférons vos articles de moyens et grands volumes de l'étranger pour le cameroun.</p>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>

    <div class="site-section bg-light">
        <div class="container">
            <div class="row align-items-stretch">
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-travel"></span></div>
                        <div>
                            <h3>Achat en ligne</h3>
                            <p>Vous désirez faire des achats en ligne mais n'avez pas de cartes? Envoyez-nous les liens des articles et nous faisons les achats pour vous.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-sea-ship-with-containers"></span></div>
                        <div>
                            <h3>Transfert de colis</h3>
                            <p>Nous transférons vos articles de moyens et grands volumes de l'étranger pour le cameroun.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-travel"></span></div>
                        <div>
                            <h3>Transport aérien</h3>
                            <p>Vos souhaitez envoyer un colis de moyen ou grand volume au cameroun? Nous sommes experts dans le transfert de marchandise par cargo.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-sea-ship-with-containers"></span></div>
                        <div>
                            <h3>Transport par Cargo</h3>
                            <p>Vous souhaitez un envoyer un colis de moyen ou grand volume au cameroun? Nous sommes experts dans le transfert de marchandise par cargo.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-platform"></span></div>
                        <div>
                            <h3>Entreposage</h3>
                            <p>Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-barn"></span></div>
                        <div>
                            <h3>Espace de rangement</h3>
                            <p>Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté.</p>
                            <p><a href="#">Lire Plus</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="site-section border-bottom">
        <div class="container">

            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="font-weight-light text-primary">Témoignages</h2>
                </div>
            </div>

            <div class="slide-one-item home-slider owl-carousel">
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_3.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Jean Tayong</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;J'ai été satisfait du service. Mon colis m'a été livré dans les délais, vous êtes très professionnels.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_2.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Christine Mballa</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;IL suffisait de vous contacter alors! Merci pour l'achat et la livraison.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_4.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Robert Mbele</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;Après une semaine seulement mon article était chez moi et en bon état.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_5.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Amélie Fosso</p>
                        </figure>
                        <blockquote>
                            <p>"Un petit retard à cause du site marchand mais je suis satisfait du service. Merci.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection