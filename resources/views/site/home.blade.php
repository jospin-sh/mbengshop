@extends('layouts.app-site')
@section('slide11')
    <div class="slider">
        <div class="slide_viewer">
            <div class="slide_group">
                <div class="slide">
                </div>
                <div class="slide">
                </div>
                <div class="slide">
                </div>
                <div class="slide">
                </div>
            </div>
        </div>
    </div><!-- End // .slider -->

    <div class="slide_buttons">
    </div>

    <div class="directional_nav">
        <div class="previous_btn" title="Previous">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="65px" height="65px" viewBox="-11 -11.5 65 66">
                <g>
                    <g>
                        <path fill="#474544" d="M-10.5,22.118C-10.5,4.132,4.133-10.5,22.118-10.5S54.736,4.132,54.736,22.118
			c0,17.985-14.633,32.618-32.618,32.618S-10.5,40.103-10.5,22.118z M-8.288,22.118c0,16.766,13.639,30.406,30.406,30.406 c16.765,0,30.405-13.641,30.405-30.406c0-16.766-13.641-30.406-30.405-30.406C5.35-8.288-8.288,5.352-8.288,22.118z"/>
                        <path fill="#474544" d="M25.43,33.243L14.628,22.429c-0.433-0.432-0.433-1.132,0-1.564L25.43,10.051c0.432-0.432,1.132-0.432,1.563,0	c0.431,0.431,0.431,1.132,0,1.564L16.972,21.647l10.021,10.035c0.432,0.433,0.432,1.134,0,1.564	c-0.215,0.218-0.498,0.323-0.78,0.323C25.929,33.569,25.646,33.464,25.43,33.243z"/>
                    </g>
                </g>
            </svg>
        </div>
        <div class="next_btn" title="Next">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="65px" height="65px" viewBox="-11 -11.5 65 66">
                <g>
                    <g>
                        <path fill="#474544" d="M22.118,54.736C4.132,54.736-10.5,40.103-10.5,22.118C-10.5,4.132,4.132-10.5,22.118-10.5	c17.985,0,32.618,14.632,32.618,32.618C54.736,40.103,40.103,54.736,22.118,54.736z M22.118-8.288	c-16.765,0-30.406,13.64-30.406,30.406c0,16.766,13.641,30.406,30.406,30.406c16.768,0,30.406-13.641,30.406-30.406 C52.524,5.352,38.885-8.288,22.118-8.288z"/>
                        <path fill="#474544" d="M18.022,33.569c 0.282,0-0.566-0.105-0.781-0.323c-0.432-0.431-0.432-1.132,0-1.564l10.022-10.035 			L17.241,11.615c 0.431-0.432-0.431-1.133,0-1.564c0.432-0.432,1.132-0.432,1.564,0l10.803,10.814c0.433,0.432,0.433,1.132,0,1.564 L18.805,33.243C18.59,33.464,18.306,33.569,18.022,33.569z"/>
                    </g>
                </g>
            </svg>
        </div>
    </div><!-- End // .directional_nav -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
@endsection
@section('slide_home')
    {{asset('images/h_slide_1.jpg')}}
@endsection

@section('slide-title')
    Facilitateur d'achat en europe et livraison au cameroun
@endsection


@section('content')
    <div class="container" style="position: relative; top: 48px;">
        <div class="row align-items-center no-gutters align-items-stretch overlap-section">
            <div class="col-md-4">
                <div class="feature-1 pricing h-100 text-center">
                    <div class="icon">
                        <span class="icon-dollar0">1</span>
                    </div>
                    <h2 class="my-4 heading">Allez sur un site marchand international</h2>
                    <p>Ouvrez dans votre navigateur les sites de vente en ligne. Ouvrez la page de l'article que vous désirez. Copiez ensuite le lien et collez le dans notre formulaire.
                        Nous vous aiderons à acheter en ligne.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-1 pricing h-100 text-center bg-dark">
                    <div class="icon">
                        <span class="icon-dollar0">2</span>
                    </div>
                    <h2 class="my-4 heading">Réglez votre facture</h2>
                    <p>Nous vous envoyons ensuite une facture de votre commande.
                        Procédez au règlement et nous effectuerons l'achat pour vous et vous recevrez vos articles en toute sécurité.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="feature-3 pricing h-100 text-center">
                    <div class="icon">
                        <span class="icon-dollar0">3</span>
                    </div>
                    <h2 class="my-4 heading">Recevez votre commande.</h2>
                    <p>Il vous reste à récuperez vos articles dans nos points de livraison.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section pd-b-0">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="mb-0 text-primary">Ce que nous offrons</h2>
                    <p class="color-black-opacity-5">Achat en Europe et livraison au Cameroun.</p>
                </div>
            </div>
            <div class="row align-items-stretch">
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-travel"></span></div>
                        <div>
                            <h3>Achat des articles à l'international</h3>
                            <p>Désormais nous rendons accessible pour vous les articles disponibles sur les plateformes marchandes internationales.
                            Nous faisons les achat en ligne pour vous et nous vous livrons au cameroun.</p>
                            <p class="mb-0"><a href="#">Voir Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-sea-ship-with-containers"></span></div>
                        <div>
                            <h3>Livraison au cameroun</h3>
                            <p>Vous avez acheter un article et souhaitez l'envoyer au cameroun? Nous nous en chargeons en toute sécurité.
                               Nos équipes acheminent pour vous tous vos colis de l'étranger pour le cameroun.</p>
                            <p class="mb-0"><a href="#">Voir Plus</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
                    <div class="unit-4 d-flex">
                        <div class="unit-4-icon mr-4"><span class="text-primary flaticon-frontal-truck"></span></div>
                        <div>
                            <h3>Transfert de colis</h3>
                            <p>Vous avez des colis à l'internationnal et désirez les acheminer au cameroun? Ne vous inqui&tez plus nous nous en chargeons</p>
                            <p class="mb-0"><a href="#">Voir Plus</a></p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="site-section block-13">
        <!-- <div class="container"></div> -->

        <div class="owl-carousel nonloop-block-13">
            <div>
                <a href="#" class="unit-1 text-center">
                    <img src="images/img_3.jpg" alt="Image" class="img-fluid">
                    <div class="unit-1-text">
                        <h3 class="unit-1-heading">Achat en ligne</h3>
                        <p class="px-5">Vous désirez faire des achats en ligne mais n'avez pas de cartes? Envoyez-nous les liens des articles et nous faisons les achats pour vous.</p>
                    </div>
                </a>
            </div>

            <div>
                <a href="#" class="unit-1 text-center">
                    <img src="images/img_6.jpg" alt="Image" class="img-fluid">
                    <div class="unit-1-text">
                        <h3 class="unit-1-heading">Espace de rangement</h3>
                        <p class="px-5">Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté.</p>
                    </div>
                </a>
            </div>

            <div>
                <a href="#" class="unit-1 text-center">
                    <img src="images/img_2.jpg" alt="Image" class="img-fluid">
                    <div class="unit-1-text">
                        <h3 class="unit-1-heading">Transport aérien</h3>
                        <p class="px-5">Pour vous, nous avons négocié les meilleurs tarifs de transport aérien. Vos colis transite vers le pays en toute sécurité.</p>
                    </div>
                </a>
            </div>
            <div>
                <a href="#" class="unit-1 text-center">
                    <img src="images/img_4.jpg" alt="Image" class="img-fluid">
                    <div class="unit-1-text">
                        <h3 class="unit-1-heading">Transport par Cargo</h3>
                        <p class="px-5">Vous souhaitez envoyer un colis de moyen ou grand volume au cameroun? Nous sommes experts dans le transfert de marchandise par cargo.</p>
                    </div>
                </a>
            </div>

            <div>
                <a href="#" class="unit-1 text-center">
                    <img src="images/img_5.jpg" alt="Image" class="img-fluid">
                    <div class="unit-1-text">
                        <h3 class="unit-1-heading">Entreposage</h3>
                        <p class="px-5">Nous nous chargeons de rassembler et d'entrposer vos colis. Ils sont tous en sécurité et vous sont envoyé en toute sureté.</p>
                    </div>
                </a>
            </div>


        </div>
    </div>

    <div class="site-section pd-b-10 pd-t-10" style="padding-bottom: 0px">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <a href="{!! route('idees_sites') !!}">
                    <h2 class="mb-0 text-primary">Idées de site d'achats</h2>
                    <p class="color-black-opacity-5">Quelques idées de sites pour vous.</p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                    <a target="blank" href="https://www.zara.com/be/fr/">
                    <div class="h-entry">
                        <img src="images/zara.jpg" alt="Image" class="img-fluid">
                        <h2 class="font-size-regular">ZARA</h2>
                        <p class="black">ZARA - MODE FEMMES</p>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                    <a target="blank"
                       href="https://fr.zalando.be/accueil-homme/?&wmc=SEM321_BR_GO._4566764697_1069983304_52041134346.&opc=2211&gclid=Cj0KCQiAtOjyBRC0ARIsAIpJyGNlxSrEgOUV77sZ3xZD7lHLE6AlzbjZXUm4hRZIZjqv-ZIrviNk4OMaAtc5EALw_wcB&gclsrc=aw.ds">
                    <div class="h-entry">
                        <img src="images/zalando.jpg" alt="Image" class="img-fluid">
                        <h2 class="font-size-regular">
                                ZALANDO</h2>
                        <p class="black">ZALANDO - MODE HOMMES</p>
                    </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                    <a target="blank" href="https://www.planetparfum.com">
                        <div class="h-entry">
                            <img src="images/planet_parfum.jpg" alt="Image" class="img-fluid">
                            <h2 class="font-size-regular">Planet Parfum</h2>
                            <p class="black">Planet Parfum - Parfum</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                    <a target="blank" href="https://www.cashconverters.be">
                        <div class="h-entry">
                            <img src="images/cashconverter.jpg" alt="Image" class="img-fluid">
                            <h2 class="font-size-regular">CASHCONVERTERS</h2>
                            <p class="black">CASHCONVERTERS - ARTICLES D'OCCASION</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                        <a target="blank" href="https://www.krefel.be/fr">
                            <div class="h-entry">
                                <img src="images/krefel.jpg" alt="Image" class="img-fluid">
                                <h2 class="font-size-regular">KREFEL</h2>
                                <p class="black">KREFEL - INFORMATIQUE</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                        <a target="blank" href="https://www.bol.com">
                            <div class="h-entry">
                                <img src="images/bol.jpg" alt="Image" class="img-fluid">
                                <h2 class="font-size-regular">BOL</h2>
                                <p class="black">BOL - COMPOSANTS ELECTRONIQUES</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                        <a target="blank" href="https://www.mister-auto.be/fr">
                            <div class="h-entry">
                                <img src="images/misterauto.jpg" alt="Image" class="img-fluid">
                                <h2 class="font-size-regular">MISTER AUTO</h2>
                                <p class="black">MISTER AUTO - AUTOMOBILE ET PIECES</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-12 mb-4 mb-lg-4">
                        <a target="blank" href="https://www.autoscout24.be/fr">
                            <div class="h-entry">
                                <img src="images/autoscout24.png" alt="Image" class="img-fluid">
                                <h2 class="font-size-regular">AUTOSCOUT24</h2>
                                <p class="black">AUTOSCOUT24 - AUTOMOBILE ET PIECES</p>
                            </div>
                        </a>
                    </div>
            </div>

        </div>
    </div>
    <div class="site-section border-bottom pd-b-10 pd-t-10">
        <div class="container">

            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="font-weight-light text-primary">Témoignages</h2>
                </div>
            </div>

            <div class="slide-one-item home-slider owl-carousel">
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_3.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Jean Tayong</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;J'ai été satisfait du service. Mon colis m'a été livré dans les délais, vous êtes très professionnels.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_2.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Christine Mballa</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;IL suffisait de vous contacter alors! Merci pour l'achat et la livraison.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_4.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Robert Mbele</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;Après une semaine seulement mon article était chez moi et en bon état.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_5.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Amélie Fosso</p>
                        </figure>
                        <blockquote>
                            <p>"Un petit retard à cause du site marchand mais je suis satisfait du service. Merci.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="site-section border-top">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12">
                    <h2 class="mb-5 text-black">Essayez nos services</h2>
                    <p class="mb-0"><a href="{!! route('order') !!}" class="btn btn-primary py-3 px-5 text-white">Commencez maintenant</a></p>
                </div>
            </div>
        </div>
    </div>

@endsection