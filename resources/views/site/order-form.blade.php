@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_4.jpg')}}
@endsection

@section('slide-title')
    Commander
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span> <span>Commander</span>
    </p>
@endsection
@section('style')
    <style>
        div.site-blocks-cover{
            background-position-y: 0px
        }
    </style>
@endsection
@section('content')
    <div class="site-section bg-light" id="scroll-point00">
        <div class="container">
            <div class="row mb-5">
                @component('helpers.alert')
                    .
                    @endcomponent

                <div class="card col-md-8 ml-auto mb-5 order-md-1" data-aos="fade" style="min-height: 937px;
">
                    <div class="card-body bg-white">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h4 class="text-primary">Remplissez le formulaire pour passer votre commande</h4>
                        </div>
                        <form class="" method="POST" action="{{ route('order.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="adresse"
                                       class="col-md-12 col-form-label">{{ __('Service') }}</label>

                                <div class="col-md-12">
                                    <select id="adresse" class="form-control @error('ville') is-invalid @enderror"
                                            name="service" autofocus required>
                                        <option value="achat">Achat et livraison</option>
                                        <option value="transfert">Transfert de colis</option>
                                    </select>

                                    @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nom" class="col-md-12 col-form-label">{{ __('Nom') }}</label>

                                <div class="col-md-12">
                                    <input id="nom" type="text" class="form-control @error('nom') is-invalid @enderror"
                                           name="nom" value="@if(auth()->user()) {{auth()->user()->nom}} @endif"
                                           required autocomplete="o,">

                                    @error('nom')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="nom" class="col-md-12 col-form-label">{{ __('Téléphone') }}</label>

                                <div class="col-md-12">
                                    <input id="telephone" type="text"
                                           value="@if(auth()->user()) {{auth()->user()->prenom}} @endif"
                                           class="form-control @error('telephone') is-invalid @enderror"
                                           name="telephone"
                                           required autocomplete="telephone" autofocus>

                                    @error('telephone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-12 col-form-label">{{ __('Adresse Mail') }}</label>

                                <div class="col-md-12">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="@if(auth()->user()) {{auth()->user()->email}} @endif"
                                           required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="ville"
                                       class="col-md-12 col-form-label">{{ __('Ville de Livraison') }}</label>

                                <div class="col-md-12">

                                    <select id="ville" type="text"
                                            class="form-control @error('ville') is-invalid @enderror" name="ville"
                                            required>
                                        <option value="douala">Douala</option>
                                    </select>

                                    @error('ville')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="adresse"
                                       class="col-md-12 col-form-label">{{ __('Point de Livraison') }}</label>

                                <div class="col-md-12">
                                    <select id="adresse" class="form-control @error('ville') is-invalid @enderror"
                                            name="adresse"
                                            required>
                                        <option value="douala">Akwa, Douala Bar</option>
                                    </select>

                                    @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="adresse"
                                       class="col-md-12 col-form-label">{{ __("J'ai juste une photo de mon article") }}</label>

                                <div class="col-md-12">
                                    <div class="radio">
                                        <label class="radio-inline control-label" for="no_image">
                                        <input type="radio" id="no_image" name="have_image" value="no"
                                               required checked>
                                            NON</label>
                                        <label class="radio-inline control-label" for="yes_image">
                                            <input type="radio" id="yes_image" name="have_image" value="yes"
                                                   required>
                                            OUI</label>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row article-group" style="display:none">
                                <label for="article[]" class="col-md-12 col-form-label">{{ __('Article') }}</label>
                                <div class="col-md-12 article-box">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="row" style="padding-left: 16px;">
                                                <input type="text"
                                                       class="form-control col-md-12 @error('article') is-invalid @enderror"
                                                       name="article_with_image[]" placeholder="Nom de l'article">
                                                <input type="text"
                                                       class="form-control @error('description') is-invalid @enderror col-md-12"
                                                       name="description_with_image[]" placeholder="Caractéristique. Exple: couleur, taille, marque...">
                                                <input type="number" min="1"
                                                       class="form-control @error('qte') is-invalid @enderror col-md-12"
                                                       name="qte_with_image[]" placeholder="Quantité. Exemple: 5">
                                                <div class="file-loading">
                                                <input id="article-img" type="file" data-preview-file-type="text" name="image"
                                                    data-browse-on-zone-click="true"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row article-group articles-no-image">
                                <label for="article[]" class="col-md-12 col-form-label">{{ __('Article') }}</label>
                                <div class="col-md-12 article-box">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <div class="row" style="padding-left: 16px;">
                                                <input type="text"
                                                       class="form-control col-md-6 @error('article') is-invalid @enderror"
                                                       name="article[]" placeholder="Lien(ou nom) de l'article" required>
                                                <input type="text"
                                                       class="form-control @error('description') is-invalid @enderror col-md-4"
                                                       name="description[]" placeholder="Caractéristique de l'article">
                                                <input type="number" min="1"
                                                       class="form-control @error('qte') is-invalid @enderror col-md-2"
                                                       name="qte[]" placeholder="qte. Exemple: 5" required>
                                            </div>

                                            @error('article')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <span class="icon icon-plus btn-add-article col-md-1"
                                              style="font-size: 1.2em; cursor: pointer; margin-top: 10px;"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary py-2 px-4 text-white">
                                        {{ __('Envoyer') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 order-md-2" data-aos="fade">
                    <div class="card">
                    <div class="p-4 bg-white">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h2 class="text-primary">Achat et livraison.</h2>
                        </div>
                        <p class="mb-5">Confiez-nous vos achats en remplissant ce formulaire.
                            Entrez vos informations et les liens de différents articles que vous désirez acheter.
                            Si vous voulez une caractéristique spécifique pour un article comme une couleur ou une taille de chaussure,
                            renseignez-le dans le champs caractéristique après le lien.
                            Remplissez aussi le nombre d'article que vous voulez.
                        </p>
                    </div>
                    <div class="p-4 bg-white">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h2 class="text-primary">Transfert de colis.</h2>
                        </div>
                        <p class="mb-5">Confiez-nous vos colis en remplissant ce formulaire.
                            Entrez vos informations et les noms des différents articles que vous désirez trasnférer au cameroun.
                            Vous pouvez préciser les caractéristiques des articles dans le champs adéquat.
                            Remplissez aussi le nombre d'article que vous voulez.
                        </p>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            var $articleImage = $("#article-img");
            var $articlesNoImages = $('.articles-no-image');
            $('#no_image').click(function(){
                $articlesNoImages.show();
                $articlesNoImages.find('input').attr('required', 'required');
                console.log('no image');
                $articleImage.closest(".form-group").hide();
                $articleImage.find('input').removeAttr('required');
            });
            $('#yes_image').click(function(){
                console.log('Kartik file Inout Loading...');
                $articlesNoImages.find('input').removeAttr('required');
                $articlesNoImages.hide();
                $articleImage.closest(".form-group").show();
                $articleImage.attr('required');
                $("#article-img").fileinput({
                    theme: "fa",
                    language: "fr",
                   /* uploadUrl: "/file-upload-batch/2", */
                    allowedFileExtensions: ["jpg", "png", "gif"]
                });
            });
        });
    </script>
    <script src="{{asset('js/script.js')}}"></script>
@endsection