@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/hero_bg_1.jpg')}}
@endsection

@section('slide-title')
    A propos
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span> <span>A propos</span></p>
@endsection

@section('content')
    <div class="site-section" id="scroll-point">
        <div class="container">
            <div class="row mb-5">

                <div class="col-md-5 ml-auto mb-5 order-md-2" data-aos="fade">
                    <img src="images/img_1.jpg" alt="Image" class="img-fluid rounded">
                </div>
                <div class="col-md-6 order-md-1" data-aos="fade">
                    <div class="text-left pb-1 border-primary mb-4">
                        <h2 class="text-primary">Qui sommes-nous?</h2>
                    </div>
                    <p>Nous sommes une brillante équipe, jeune et dynamique engagé à vous servir. Nous mettons à votre disposition des services de transfert
                        de colis, et d'achat en ligne. Votre satisfaction demeure notre principale priorité.</p>
                    <p class="mb-5">Désormais une pléthores d'article sont à votre portée. <i>Mbeng</i> est tout proche du cameroun et vice-versa grâce à Mbenshop.
                        Nous achetons pour vous, pas besoin de carte de crédit. De même nous transférons vos colis à vos proches depuis l'étranger.</p>
                    <div class="row">
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6">
                            <div class="unit-4">
                                <div class="unit-4-icon mb-3 mr-4"><span class="text-primary flaticon-travel"></span>
                                </div>
                                <div>
                                    <h3>Achat à l'international</h3>
                                    <p>Nous achetons pour vous des articles sur des platformes marchandes internationales.
                                        Désormais les achats sont plus faciles, envoyez-nous les liens et nous faisons les achats
                                        et nous vous livrons au cameroun.</p>
                                    <p class="mb-0"><a href="#">Lire la suite</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-md-5 mb-0 col-lg-6">
                            <div class="unit-4">
                                <div class="unit-4-icon mb-3 mr-4"><span
                                            class="text-primary flaticon-frontal-truck"></span></div>
                                <div>
                                    <h3>Livraison au cameroun</h3>
                                    <p>Vous désirez transférer au pays des colis depuis l'extérieur, nous nous en chargeons. Contactez-nous, nous acheminons vos colis au Cameroun.</p>
                                    <p class="mb-0"><a href="#">Lire la suite</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="site-section bg-image overlay" style="background-image: url('images/hero_bg_4.jpg');">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="font-weight-light text-primary" data-aos="fade">Comment ça marche?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
                    <div class="how-it-work-item">
                        <span class="number">1</span>
                        <div class="how-it-work-body">
                            <h2>Visitez la plateforme marchande de votre choix</h2>
                            <p class="mb-5">Rendes-vous sur le site marchand sur lequel vous avez repérer un article que vous voulez acheter.
                                Cliquez sur le lien de l'article que vous voulez acheter. Copiez ensuite le lien dans la barre d'adresse en haut de votre navigateur.
                            Ensuite revenez sur Mbengshop et ouvrez la page 'achat' du menu service. Remplissez alors le formulaire et votre commande sera enregistrée.</p>
                            <ul class="ul-check list-unstyled white">
                                <li class="text-white">Visitez un site e-commerce et copiez le lien de l'article.</li>
                                <li class="text-white">Copiez le lien de l'article sur ce site.</li>
                                <li class="text-white">Revenez sur notre site et remplissez le formulaire de commande.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
                    <div class="how-it-work-item">
                        <span class="number">2</span>
                        <div class="how-it-work-body">
                            <h2>Réglez votre facture</h2>
                            <p class="mb-5">Une fois votre commande reçue, nous vous enverrons une facture qui la récapitula.
                                Vous n'aurez qu'à la reglez pour déclencher le processus d'achat. Puis nous effectuerons l'achat sur la plateforme e-commerce pour vous.
                            La facturation contient les frais de douane et d'acheminement au pays.
                                <br>
                                <br>
                                <br>
                                <br>
                            </p>
                            <ul class="ul-check list-unstyled white">
                                <li class="text-white">Nous vous enverons une facture pro(format pour votre commande.</li>
                                <li class="text-white">Vous payerez par le moyen qui vous convient.</li>
                                <li class="text-white">Nous confirmerons votre transaction.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
                    <div class="how-it-work-item">
                        <span class="number">3</span>
                        <div class="how-it-work-body">
                            <h2>Récupérez votre colis</h2>
                            <p class="mb-5">Dès lors que votre article est payé et livré par le site marchand paeiement effectué, nous achetonss pour vous les articles.
                            Par la suite, votre colis est acheminé au cameroun et vous le recuperez dans un de nos points de livraison.
                                <br>
                                <br>
                                <br>
                                <br>
                            </p>
                            <ul class="ul-check list-unstyled white">
                                <li class="text-white">Nous achetons sur la plateforme marchande.</li>
                                <li class="text-white">Nous recevons les produits de l'agence e-commerce et le transferons au pays</li>
                                <li class="text-white">Vous récupererez votre colis dans un de nos points de livraison.</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="site-section border-bottom">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="font-weight-light text-primary" data-aos="fade">NOTRE EQUIPE</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
                    <div class="person">
                        <img src="images/person_2.jpg" alt="Image" class="img-fluid rounded mb-5">
                        <h3>Christine Rooster</h3>
                        <p class="position text-muted">Co-Founder, President</p>
                        <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi at consequatur
                            unde molestiae quidem provident voluptatum deleniti quo iste error eos est praesentium
                            distinctio cupiditate tempore suscipit inventore deserunt tenetur.</p>
                        <ul class="ul-social-circle">
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-linkedin"></span></a></li>
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
                    <div class="person">
                        <img src="images/person_3.jpg" alt="Image" class="img-fluid rounded mb-5">
                        <h3>Brandon Sharp</h3>
                        <p class="position text-muted">Co-Founder, COO</p>
                        <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi at consequatur
                            unde molestiae quidem provident voluptatum deleniti quo iste error eos est praesentium
                            distinctio cupiditate tempore suscipit inventore deserunt tenetur.</p>
                        <ul class="ul-social-circle">
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-linkedin"></span></a></li>
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
                    <div class="person">
                        <img src="images/person_4.jpg" alt="Image" class="img-fluid rounded mb-5">
                        <h3>Connor Hodson</h3>
                        <p class="position text-muted">Marketing</p>
                        <p class="mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi at consequatur
                            unde molestiae quidem provident voluptatum deleniti quo iste error eos est praesentium
                            distinctio cupiditate tempore suscipit inventore deserunt tenetur.</p>
                        <ul class="ul-social-circle">
                            <li><a href="#"><span class="icon-facebook"></span></a></li>
                            <li><a href="#"><span class="icon-twitter"></span></a></li>
                            <li><a href="#"><span class="icon-linkedin"></span></a></li>
                            <li><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section border-bottom">
        <div class="container">

            <div class="row justify-content-center mb-5">
                <div class="col-md-7 text-center border-primary">
                    <h2 class="font-weight-light text-primary">Témoignages</h2>
                </div>
            </div>

            <div class="slide-one-item home-slider owl-carousel">
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_3.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Jean Tayong</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;J'ai été satisfait du service. Mon colis m'a été livré dans les délais, vous êtes très professionnels.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>
                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_2.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Christine Mballa</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;IL suffisait de vous contacter alors! Merci pour l'achat et la livraison.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_4.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Robert Mbele</p>
                        </figure>
                        <blockquote>
                            <p>&ldquo;Après une semaine seulement mon article était chez moi et en bon état.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

                <div>
                    <div class="testimonial">
                        <figure class="mb-4">
                            <img src="images/person_5.jpg" alt="Image" class="img-fluid mb-3">
                            <p>Amélie Fosso</p>
                        </figure>
                        <blockquote>
                            <p>"Un petit retard à cause du site marchand mais je suis satisfait du service. Merci.&rdquo;</p>
                        </blockquote>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection