@extends('layouts.app-site')

@section('content')
    <div class="site-section bg-light" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="p-4 mb-3 bg-white">
                        <h3 class="h5 text-black mb-3">Dev Passion Academy</h3>
                        <p>Contactez-nous pour la réalisation de vos applications web et mobiles professionels. </p>
                    </div>
                    <div class="p-4 mb-3 bg-white">
                        <p class="mb-0 font-weight-bold">Téléphone</p>
                        <p class="mb-4"><a href="#">+237 694680790/+237 650027659</a></p>

                        <p class="mb-0 font-weight-bold">Email</p>
                        <p class="mb-0"><a href="#">devpassionacademy@gmail.com</a></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection