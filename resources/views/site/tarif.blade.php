@extends('layouts.app-site')

@section('slide-image')
    {{asset('images/slide_5.jpg')}}
@endsection

@section('slide-title')
    Nos tarifs
@endsection

@section('breadcrumb')
    <p class="breadcrumb-custom"><a href="{!! route('home') !!}">Accueil</a> <span class="mx-2">&gt;</span> <span>Nos Tarifs</span>
    </p>
@endsection
@section('style')
    <style>
        div.site-blocks-cover{
            background-position-y: 0px
        }
    </style>
@endsection
@section('content')
    <div class="site-section bg-light" id="scroll-point">
        <div class="container">
            <div class="row mb-5">

                <div class="card col-md-7 ml-auto mb-5 order-md-1" data-aos="fade" style="min-height: 609px;">
                    <div class="card-body bg-white">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h4 class="text-primary">Remplissez le formulaire pour estimer votre commande.</h4>
                        </div>
                        <form class="" method="POST" action="{{ route('order.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="adresse"
                                       class="col-md-12 col-form-label">{{ __('Service') }}</label>

                                <div class="col-md-12">
                                    <select id="service" class="form-control @error('ville') is-invalid @enderror"
                                            name="service"
                                            required>
                                        <option value="achat">Achat et livraison</option>
                                        <option value="transfert">Transfert de colis</option>
                                    </select>

                                    @error('adresse')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nom" class="col-md-12 col-form-label">{{ __('Pays') }}</label>

                                <div class="col-md-12">
                                    <select id="pays" class="form-control @error('pays') is-invalid @enderror"
                                            name="nom" required autofocus>
                                        <option value="belgique">Belgique</option>
                                    </select>

                                    @error('belgique')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="nom" class="col-md-12 col-form-label">{{ __('Prix de l\'article') }} (<span class="icon-euro"></span>)</label>
                                <div class="col-md-12">
                                    <input type="number" min="0" step="0.1"
                                           class="form-control @error('prix') is-invalid @enderror"
                                           name="prix" id="prix" placeholder="prix sur le site marchand" required/>

                                    @error('prix')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="orm-group row">
                                <label for="nom" class="col-md-12 col-form-label">{{ __('Volume de l\'article') }} <strong>(KG)</strong></label>
                                <div class="col-md-12">
                                    <input type="number" min="0" step="0.1"
                                           class="form-control @error('prix') is-invalid @enderror"
                                           name="volume" id="volume" placeholder="volume de l'article" required/>

                                    @error('volume')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <br>
                            <div class="form-group row mb-0">
                                <div class="col-md-6">
                                    <button type="submit" id="btn-tarif" class="btn btn-primary py-2 px-4 text-white">
                                        {{ __('Calculer') }}
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" disabled class="btn blue py-2 px-4 text-white">
                                        Prix: <span id="tarif-resultat"></span> FCFA
                                    </button>
                                </div>
                            </div>
                            <br/>
                        </form>
                    </div>
                </div>
                <div class="col-md-5 order-md-2" data-aos="fade">
                    <div class="card p-4 bg-white">
                        <div class="card-body">
                            <div class="text-left pb-1 border-primary mb-4">
                                <h2 class="text-primary">Facturation.</h2>
                            </div>
                            <div class="mb-5">
                                Le montant que vous payez est calculé selon la formule suivante:
                                <p class="font-weight-bold font-italic">
                                    Coût de l'article sur le site + Frais Livraison au Cameroun + Frais de Service Mbengshop
                                </p>
                                <p>
                                    Coût de l'article sur le site = Coût total facturé par le site marchand c’est-à-dire le prix de l’article et le coût de livraison de l’article à notre adresse;
                                    <br/>
                                    <br/>
                                    Frais Livraison au Cameroun = 6500 XAF/KG ;
                                    <br/>
                                    <br/>
                                    Commissions Mbenshop et Add-On Douane = 10% du coût total de l'article.
                                </p>

                            </div>
                        </div>
                    </div>
                    <!--div class="p-4 bg-white">
                        <div class="text-left pb-1 border-primary mb-4">
                            <h2 class="text-primary">Transfert de colis.</h2>
                        </div>
                        <p class="mb-5">Confiez-nous vos colis en remplissant ce formulaire.
                            Entrez vos informations et le noms des différents articles que vous désirez trasnférer au
                            cameroun.
                            Vous pouvez oréciser les caractéristiques des articles dans le champ adéquat.
                            Remplissez aussi le nombre d'article que vous voulez.
                        </p>
                    </div>
                </div-->

            </div>
        </div>
    </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
@endsection