<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mbeng shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="{{asset('images/logo.jpeg')}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">

    <link rel="stylesheet" href={{asset('theme/fonts/icomoon/style.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/bootstrap.min.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/magnific-popup.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/jquery-ui.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/owl.carousel.min.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/owl.theme.default.min.css')}}>

    <link rel="stylesheet" href={{asset('theme/css/bootstrap-datepicker.css')}}>

    <link rel="stylesheet" href={{asset('theme/fonts/flaticon/font/flaticon.css')}}>


    <link rel="stylesheet" href={{asset('theme/css/aos.css')}}>

    <link rel="stylesheet" href={{asset('theme/css/style.css')}}>
    <link rel="stylesheet" href={{asset('css/style.css')}}>
</head>
<body>

<div class="site-wrap">
    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-3" role="banner">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-10 col-xl-2">
                    <img class="logo" style="width: 52%" src="{{asset('images/logo.png')}}"/>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">

                        <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                            <li class="@if(request()->routeIs('home*')) active @endif"><a href={!! route('homepage') !!}>Accueil</a></li>
                            <li class="@if(request()->routeIs('about*')) active @endif"><a href={!! route('about') !!}>A propos</a></li>
                            <li class="@if(request()->routeIs('services*')) active @endif">
                                <a href={!! route('services') !!}>Nos Services</a>
                            </li>
                            <li class="@if(request()->routeIs('order*')) active @endif"><a href={!! route('order') !!}>Commander</a></li>
                            <li class="@if(request()->routeIs('tarif*')) active @endif"><a href={!! route('tarif') !!}>Nos Tarifs</a></li>
                            <li class="@if(request()->routeIs('contact*')) active @endif"><a href={!! route('contact') !!}>Contactez Nous</a></li>
                            @guest
                            <li class="@if(request()->routeIs('site.login*') || request()->routeIs('site.register*')) active @endif">
                                <a href={!! route('site.login') !!}>Connnexion/ Inscription</a>
                            </li>
                            @else
                                <li>
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ auth()->user()->nom }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Déconnexion') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                                @endguest
                        </ul>
                    </nav>
                </div>

                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a
                            href="#" class="site-menu-toggle js-menu-toggle text-white"><span
                                class="icon-menu h3" style="background: silver;"></span></a></div>

            </div>

        </div>
    </header>
</div>
    @yield('content')

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Liens rapides</h2>
                        <ul class="list-unstyled">
                            <li><a href="{!! route('about') !!}">A propos</a></li>
                            <li><a href="{!! route('services') !!}">Nos Services</a></li>
                            <li><a href="#">Témoignages</a></li>
                            <li><a href="{!! route('contact') !!}">Contactez Nous</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Contactez-nous</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="icon-phone"> (+237) 697502771 / 675972378</a>
                            </li>
                            <li><a href="#" class="icon-whatsapp">+32 466 83 14 61 </a>
                            </li>
                            <li><a href="#" class="icon-contact_mail" style="width: 104%;"> contact@mbengshop.com</a>
                            </li>
                            <li><a target="_blank" href="https://www.facebook.com/Mbengshop-102046668080345" class="icon-facebook"> Facebook</a>
                            </li>
                            <li><a target="_blank" href="https://www.instagram.com/p/B9UHoWuHRf0/?igshid=1m4i6of0podif" class="icon-instagram"> Instagram</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Moyens de paiement</h2>
                        <ul class="list-unstyled">
                            <li><a href="{!! route('mode_paiement') !!}/#cash">
                                    <img class="logo-payement" src="{{asset('images/cash.jpg')}}" alt=""> Espèce</a>7
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#eumm">
                                    <img src="{{asset('images/eumm.png')}}" alt=""> EU Mobile Money</a>7
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#om">
                                    <img src="{{asset('images/om.png')}}" alt=""/> Orange Money</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#mtn">
                                    <img src="{{asset('images/mtn.png')}}" alt=""/> MTN Mobile Money</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#freecash">
                                    <img src="{{asset('images/freecash.png')}}" alt=""/> FreeCash</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#paypal">
                                    <img class="logo-payement" src="{{asset('images/paypal.png')}}" alt=""
                                         style="width: 18px;"/> PayPal</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#paypal">
                                    <img class="logo-payement" src="{{asset('images/visa.png')}}" alt=""
                                         style="width: 18px;"/> Visa</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#paypal">
                                    <img class="logo-payement" src="{{asset('images/mastercard.png')}}" alt=""
                                         style="width: 18px;"/> MasterCard</a>
                            </li>
                        </ul>
                    </div>
                    <!--div class="col-md-3">
                        <h2 class="footer-heading mb-4">Suivez-nous</h2>
                        <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                    </div-->
                </div>
            </div>
            <div class="col-md-3">
                <h2 class="footer-heading mb-4">Abonnez-vous à la newsletter</h2>
                <form action="#" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control border-secondary text-white bg-transparent"
                               placeholder="Entez votre email" aria-label="Entez votre email"
                               aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary text-white" type="button" id="button-addon2">OK</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        Tous droits réservés | Ce template est fait avec <i class="icon-heart" aria-hidden="true"></i>
                        par <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                    <p><a target="_blank" href="{!! route('contact_dev') !!}">Besoin d'un site internet professionnel?
                            Cliquez ici!</a></p>
                </div>
            </div>

        </div>
    </div>
</footer>
<script src={{asset('theme/js/jquery-3.3.1.min.js')}}></script>
<script src={{asset('theme/js/jquery-migrate-3.0.1.min.js')}}></script>
<script src={{asset('theme/js/jquery-ui.js')}}></script>
<script src={{asset('theme/js/popper.min.js')}}></script>
<script src={{asset('theme/js/bootstrap.min.js')}}></script>
<script src={{asset('theme/js/owl.carousel.min.js')}}></script>
<script src={{asset('theme/js/jquery.stellar.min.js')}}></script>
<script src={{asset('theme/js/jquery.countdown.min.js')}}></script>
<script src={{asset('theme/js/jquery.magnific-popup.min.js')}}></script>
<script src={{asset('theme/js/bootstrap-datepicker.min.js')}}></script>
<script src={{asset('theme/js/aos.js')}}></script>

<script src={{asset('theme/js/main.js')}}></script>
@yield('script')
</body>
</html>