<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mbeng shop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" href="{{asset('images/logo.png')}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700">

    <link rel="stylesheet" href={{asset('theme/fonts/icomoon/style.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/bootstrap.min.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/magnific-popup.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/jquery-ui.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/owl.carousel.min.css')}}>
    <link rel="stylesheet" href={{asset('theme/css/owl.theme.default.min.css')}}>

    <link rel="stylesheet" href={{asset('theme/css/bootstrap-datepicker.css')}}>

    <link rel="stylesheet" href={{asset('theme/fonts/flaticon/font/flaticon.css')}}>
    <link rel="stylesheet" href={{asset("css/font-awesome-4.7.0/css/font-awesome.min.css")}}>


    <link rel="stylesheet" href={{asset('theme/css/aos.css')}}>

    <link rel="stylesheet" href={{asset('theme/css/style.css')}}>
    <link rel="stylesheet" href={{asset('css/style.css')}}>
    <link rel="stylesheet" href={{asset('css/slider.css')}}>
    <link rel="stylesheet" href={{asset('css/tabs.css')}}>
    <link rel="stylesheet" href={{asset('js/kartik-v-bootstrap-fileinput/css/fileinput.css')}}>

</head>
<body>
@yield('style')
<div class="site-wrap">

    <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
            <div class="site-mobile-menu-close mt-3">
                <span class="icon-close2 js-menu-toggle" style="background: silver;"></span>
            </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar py-3" role="banner" style="">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-10 col-xl-2">
                    <img class="logo" style="width: 52%" src="{{asset('images/logo.png')}}"/>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">

                        <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                            <li class="@if(request()->routeIs('home*')) active @endif"><a href={!! route('homepage') !!}>Accueil</a></li>
                            <li class="has-children @if(request()->routeIs('services*') || request()->routeIs('about*')) active @endif">
                            <li class="has-children @if(request()->routeIs('services*') || request()->routeIs('about*')) active @endif">
                                <a href={!! route('services') !!}>Nos Services</a>
                                <ul class="dropdown">
                                    <li><a href={!! route('services') !!}>Nos Services</a></li>
                                    <li class="@if(request()->routeIs('about*')) active @endif"><a href={!! route('about') !!}>A propos</a></li>
                                </ul>
                            </li>
                            <li class="@if(request()->routeIs('idees_sites*')) active @endif"><a href={!! route('idees_sites') !!}>Idées de sites</a></li>
                            <li class="@if(request()->routeIs('order*')) active @endif"><a href={!! route('order') !!}>Commander</a></li>
                            <li class="@if(request()->routeIs('tarif*')) active @endif"><a href={!! route('tarif') !!}>Nos Tarifs</a></li>
                            <li class="@if(request()->routeIs('contact*')) active @endif"><a href={!! route('contact') !!}>Contactez Nous</a></li>
                            @guest
                            <li class="has-children @if(request()->routeIs('site.login*') || request()->routeIs('site.register*')) active @endif">
                                <a href={!! route('site.login') !!}>Connexion</a>
                                <ul class="dropdown">
                                    <li><a href={!! route('site.login') !!}>Connexion</a></li>
                                    <li class="@if(request()->routeIs('site.login*')) active @endif"><a href={!! route('site.login') !!}>Inscription</a></li>
                                </ul>
                            </li>
                            @else
                                <li>
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ auth()->user()->nom }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Déconnexion') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </nav>
                </div>

                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a
                            href="#" class="site-menu-toggle js-menu-toggle text-white"><span
                                class="icon-menu h3" style="background: silver;"></span></a></div>
            </div>

        </div>
    </header>
</div>
@hasSection("slide_home")
<div class="slider">
    <div class="slide_viewer">
        <div class="slide_group">
            <div class="slide site-blocks-cover overlay" style="background-image: url({{asset("images/h_slide_1.jpg")}});
                    background-repeat: no-repeat; background-position: 50% 172px!important;" data-aos="fade"
                data-stellar-background-ratio="0.5">
                <div class="container">
                    <div class="row align-items-center justify-content-center text-center">
                        <div class="col-md-8 col-xs-10" data-aos="fade-up" data-aos-delay="400">
                            <h1 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold title top-title" style="">
                                @yield('slide-title') </h1>
                                <p><a href="{!! route('order') !!}" class="btn py-3 px-5 text-white btn-border-white" style="">Commencez!</a></p>
                            @yield('breadcrumb')

                        </div>
                    </div>
                </div>
            </div>
        <div class="slide site-blocks-cover overlay"
             style="background-image: url({{asset("images/h_slide_2.jpg")}}); background-position: 50% -2px!important;" data-aos="fade"
             data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center text-center">
                    <div class="col-md-8 col-xs-10" data-aos="fade-up" data-aos-delay="400">
                            <p><a href="{!! route('order') !!}" class="btn py-3 px-5 text-white btn-border-white" style="">Commencez!</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide site-blocks-cover overlay" style="background-image: url({{asset("images/h_slide_3.jpg")}});" data-aos="fade"
             data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="row align-items-center justify-content-center text-center">
                    <div class="col-md-8 col-xs-10" data-aos="fade-up" data-aos-delay="400">
                        <p><a href="{!! route('order') !!}" class="btn py-3 px-5 text-white btn-border-white" style="">Commencez!</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide site-blocks-cover overlay" style="" data-aos="fade"
                 data-stellar-background-ratio="1">
            <video autoplay muted loop id="myVideo" style="width: 100%; height: auto;">
                <source src="{{asset("images/h_slide_4.mp4")}}" type="video/mp4">
            </video>
        </div>
    </div>
</div>
<div class="slide_buttons"></div>
<div class="directional_nav">
    <div class="previous_btn"><span class="icon-keyboard_arrow_left"></span></div>
    <div class="next_btn"><span class="icon-keyboard_arrow_right"></span></div>
</div><!-- End // .directional_nav -->
@endif
@hasSection("slide")
    <div class="site-blocks-cover overlay slide" style="background-image: url(@yield('slide_image')}});" data-aos="fade"
         data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8 col-xs-10" data-aos="fade-up" data-aos-delay="400">
                    <h1 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold title" style="">
                        @yield('slide-title') </h1>
                    @hasSection('slide-start-button')
                        <p><a href="{!! route('order') !!}" class="btn py-3 px-5 text-white btn-border-white" style="">Commencez!</a></p>
                    @endif

                    @yield('breadcrumb')

                </div>
            </div>
        </div>
    </div>
@endif

@yield('content')

<footer class="site-footer" style="padding: 2em;">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Liens rapides</h2>
                        <ul class="list-unstyled">
                            <li><a href="{!! route('about') !!}">A propos</a></li>
                            <li><a href="{!! route('services') !!}">Nos Services</a></li>
                            <li><a href="#">Témoignages</a></li>
                            <li><a href="{!! route('contact') !!}">Contactez Nous</a></li>
                            <li><a href="{!! route('condition_vente') !!}">Conditions de vente</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Contactez-nous</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="icon-phone"> (+237) 697502771 / 675972378</a>
                            </li>
                            <li><a href="#" class="icon-whatsapp"> +32 466 83 14 61 </a>
                            </li>
                            <li><a href="#" class="icon-contact_mail" style="width: 104%;"> contact@mbengshop.com</a>
                            </li>
                            <li><a target="_blank" href="https://www.facebook.com/Mbengshop-102046668080345" class="icon-facebook"> Facebook</a>
                            </li>
                            <li><a target="_blank" href="https://www.instagram.com/p/B9UHoWuHRf0/?igshid=1m4i6of0podif" class="icon-instagram"> Instagram</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h2 class="footer-heading mb-4">Moyens de paiement</h2>
                        <ul class="list-unstyled">
                            <li><a href="{!! route('mode_paiement') !!}/#eu">
                                    <img class="logo-payement" src="{{asset('images/cash.jpg')}}" alt=""> Espèce</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#eu">
                                    <img src="{{asset('images/eumm.png')}}" alt=""> EU Mobile Money</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#eu">
                                    <img src="{{asset('images/om.png')}}" alt=""/> Orange Money</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#eu">
                                    <img src="{{asset('images/mtn.png')}}" alt=""/> MTN Mobile Money</a>
                            </li>
                            <!--li><a href="{ route('mode_paiement') !!}/#om">
                                    <img src="asset('images/freecash.png')}}" alt=""/> FreeCash</a>
                            </li-->
                            <li><a href="{!! route('mode_paiement') !!}/#eu">
                                    <img class="logo-payement" src="{{asset('images/paypal.png')}}" alt="" style="width: 18px;"/> PayPal</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#paypal">
                                    <img class="logo-payement" src="{{asset('images/visa.png')}}" alt=""
                                         style="width: 18px;"/> Visa</a>
                            </li>
                            <li><a href="{!! route('mode_paiement') !!}/#paypal">
                                    <img class="logo-payement" src="{{asset('images/mastercard.png')}}" alt=""
                                         style="width: 18px;"/> MasterCard</a>
                            </li>
                        </ul>
                    </div>
                    <!--div class="col-md-3">
                        <h2 class="footer-heading mb-4">Suivez-nous</h2>
                        <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                        <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                    </div-->
                </div>
            </div>
            <div class="col-md-3">
                <h2 class="footer-heading mb-4">Abonnez-vous à la newsletter</h2>
                <form action="#" method="post">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control border-secondary text-white bg-transparent"
                               placeholder="Entez votre email" aria-label="Entez votre email" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary text-white" type="button" id="button-addon2">OK</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12">
                <div class="border-top pt-5">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        Tous droits réservés | Ce template est fait avec <i class="icon-heart" aria-hidden="true"></i>
                        par <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                    <p><a target="_blank" href="https://devpassionacademy.com">Réalisation: DevPassionAcademy</a></p>
                </div>
            </div>

        </div>
    </div>
</footer>

<script src={{asset('theme/js/jquery-3.3.1.min.js')}}></script>
<script src={{asset('theme/js/jquery-migrate-3.0.1.min.js')}}></script>
<script src={{asset('theme/js/jquery-ui.js')}}></script>
<script src={{asset('theme/js/popper.min.js')}}></script>
<script src={{asset('theme/js/bootstrap.min.js')}}></script>
<script src={{asset('theme/js/owl.carousel.min.js')}}></script>
<script src={{asset('theme/js/jquery.stellar.min.js')}}></script>
<script src={{asset('theme/js/jquery.countdown.min.js')}}></script>
<script src={{asset('theme/js/jquery.magnific-popup.min.js')}}></script>
<script src={{asset('theme/js/bootstrap-datepicker.min.js')}}></script>
<script src={{asset('theme/js/aos.js')}}></script>
<script src={{asset('js/kartik-v-bootstrap-fileinput/js/plugins/piexif.js')}}></script>
<script src={{asset('js/kartik-v-bootstrap-fileinput/js/fileinput.js')}}></script>
<script src={{asset('js/kartik-v-bootstrap-fileinput/js/locales/fr.js')}}></script>
<script src={{asset('js/kartik-v-bootstrap-fileinput/themes/fa/theme.js')}}></script>

<script src={{asset('theme/js/main.js')}}></script>
<script src={{asset('js/slider.js')}}></script>
<script type="text/javascript">
    /* Scroll the page on a specific div */
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#scroll-point').offset().top
        }, '3000');
    });
</script>
    </div>
</div>
@yield('script')
</body>
</html>