<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href={{asset('theme-admin/assets/img/apple-icon.png')}}>
    <link rel="icon" type="image/png" href="{{asset('images/logo.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!-- Scripts -->
    <script src="{{-- asset('js/app.js') --}}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{asset('theme-admin/assets/css/material-dashboard.css?v=2.1.1')}}" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('theme-admin/assets/demo/demo.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href={{asset('theme/fonts/icomoon/style.css')}}>
</head>
<body>
<div class="wrapper ">
    <style>
        .article-img{
            width: 250px!important;
            height: auto;
        }
        .rd-btn{
            border-radius: 25px;
        }
        .red{
            background: #e91e63;
        }
        .green{
            background: #18ce0f;
        }
    </style>
    @auth
    <div class="sidebar" data-color="purple" data-background-color="white"
         data-image={{ asset('theme-admin/assets/img/sidebar-1.jpg')}}>
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text logo-normal">
                <img class="logo" style="width: 45%; margin-left: 30%" src="{{asset('images/logo.png')}}"/>
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <!--li class="nav-item  ">
                    <a class="nav-link" href="./dashboard.html">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li-->
                <!--li class="nav-item ">
                    <a class="nav-link" href="">
                        <i class="material-icons">person</i>
                        <p>Utilisateur</p>
                    </a>
                </li-->
                <li class="nav-item active ">
                    <a class="nav-link" href="{!! route('commande') !!}">
                        <i class="material-icons">content_paste</i>
                        <p>Commandes</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{!! route('commande') !!}">
                        <i class="material-icons">content_paste</i>
                        <p>Facture</p>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    @endauth
    <div class="main-panel">
    @auth
    <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="#pablo">@yield('title')</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    <ul class="navbar-nav">
                        <!-- Authentication Links -->
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                        @else

                            @endguest
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">person</i>
                                    <p class="d-lg-none d-md-block">
                                        Account
                                    </p>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Déconnexion') }}({{auth()->user()->nom}})
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        @endauth
        <div class="content" style="padding-top: 0;">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <nav class="float-left">
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Mbeng-shop
                            </a>
                        </li>
                        <li>
                            <a href={!! route('commande') !!}>
                                Commande
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    , made with <i class="material-icons">favorite</i> by
                    <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
                </div>
            </div>
        </footer>
    </div>
</div>
<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>
        <ul class="dropdown-menu">
            <li class="header-title"> Sidebar Filters</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger active-color">
                    <div class="badge-colors ml-auto mr-auto">
                        <span class="badge filter badge-purple" data-color="purple"></span>
                        <span class="badge filter badge-azure" data-color="azure"></span>
                        <span class="badge filter badge-green" data-color="green"></span>
                        <span class="badge filter badge-warning" data-color="orange"></span>
                        <span class="badge filter badge-danger" data-color="danger"></span>
                        <span class="badge filter badge-rose active" data-color="rose"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li class="header-title">Images</li>
            <li class="active">
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src={{asset('theme-admin/assets/img/sidebar-1.jpg')}} alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src={{asset('theme-admin/assets/img/sidebar-2.jpg')}} alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src={{asset('theme-admin/assets/img/sidebar-3.jpg')}} alt="">
                </a>
            </li>
            <li>
                <a class="img-holder switch-trigger" href="javascript:void(0)">
                    <img src={{ asset('theme-admin/assets/img/sidebar-4.jpg')}} alt="">
                </a>
            </li>
            <li class="button-container">
                <a href="https://www.creative-tim.com/product/material-dashboard" target="_blank"
                   class="btn btn-primary btn-block">Free Download</a>
            </li>
            <!-- <li class="header-title">Want more components?</li>
                <li class="button-container">
                    <a href="https://www.creative-tim.com/product/material-dashboard-pro" target="_blank" class="btn btn-warning btn-block">
                      Get the pro version
                    </a>
                </li> -->
            <li class="button-container">
                <a href="https://demos.creative-tim.com/material-dashboard/docs/2.1/getting-started/introduction.html"
                   target="_blank" class="btn btn-default btn-block">
                    View Documentation
                </a>
            </li>
            <li class="button-container github-star">
                <a class="github-button" href="https://github.com/creativetimofficial/material-dashboard"
                   data-icon="octicon-star" data-size="large" data-show-count="true"
                   aria-label="Star ntkme/github-buttons on GitHub">Star</a>
            </li>
            <li class="header-title">Thank you for 95 shares!</li>
            <li class="button-container text-center">
                <button id="twitter" class="btn btn-round btn-twitter"><i class="fa fa-twitter"></i> &middot; 45
                </button>
                <button id="facebook" class="btn btn-round btn-facebook"><i class="fa fa-facebook-f"></i> &middot; 50
                </button>
                <br>
                <br>
            </li>
        </ul>
    </div>
</div>
<!--   Core JS Files   -->
<script src={{ asset('theme-admin/assets/js/core/jquery.min.js')}}></script>
<script src={{ asset('theme-admin/assets/js/core/popper.min.js')}}></script>
<script src={{ asset('theme-admin/assets/js/core/bootstrap-material-design.min.js')}}></script>
<script src={{ asset('theme-admin/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}></script>
<!-- Plugin for the momentJs  -->
<script src={{ asset('theme-admin/assets/js/plugins/moment.min.js')}}></script>
<!--  Plugin for Sweet Alert -->
<script src={{ asset('theme-admin/assets/js/plugins/sweetalert2.js')}}></script>
<!-- Forms Validations Plugin -->
<script src={{ asset('theme-admin/assets/js/plugins/jquery.validate.min.js')}}></script>
<!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src={{ asset('theme-admin/assets/js/plugins/jquery.bootstrap-wizard.js')}}></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src={{ asset('theme-admin/assets/js/plugins/bootstrap-selectpicker.js')}}></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src={{ asset('theme-admin/assets/js/plugins/bootstrap-datetimepicker.min.js')}}></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
<script src={{ asset('theme-admin/assets/js/plugins/jquery.dataTables.min.js')}}></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src={{ asset('theme-admin/assets/js/plugins/bootstrap-tagsinput.js')}}></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src={{ asset('theme-admin/assets/js/plugins/jasny-bootstrap.min.js')}}></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src={{ asset('theme-admin/assets/js/plugins/fullcalendar.min.js')}}></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src={{ asset('theme-admin/assets/js/plugins/jquery-jvectormap.js')}}></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src={{ asset('theme-admin/assets/js/plugins/nouislider.min.js')}}></script>
<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<!-- Library for adding dinamically elements -->
<script src={{ asset('theme-admin/assets/js/plugins/arrive.min.js')}}></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Chartist JS -->
<script src={{ asset('theme-admin/assets/js/plugins/chartist.min.js')}}></script>
<!--  Notifications Plugin    -->
<script src={{ asset('theme-admin/assets/js/plugins/bootstrap-notify.js')}}></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script src={{ asset("theme-admin/assets/js/material-dashboard.js?v=2.1.1")}} type="text/javascript"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src={{ asset('theme-admin/assets/demo/demo.js')}}></script>
@yield('script')
</body>
</html>
