<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clear Config cache:
Route::get('/clear', 'CacheController@clear')->name('clear');
Route::get('/cache', 'CacheController@cache')->name('cache');

Route::group(['prefix'=>'admin', 'middleware'=>'admin'], function() {
    Route::group(['middleware'=>'auth'], function(){
        Route::get('/facture/{id}', 'CommandeController@facture')->name('facture');
        Route::post('/send-facture/{id}', 'CommandeController@sendFacture')->name('facture.send');
        Route::get('/commande', 'CommandeController@index')->name('commande');
        Route::get('/commande/{id}', 'CommandeController@show')->name('commande.show');
    });
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('admin_home');
    Auth::routes();
});

Route::get('/', 'SiteController@index')->name('homepage');
Route::get('/apropos', 'SiteController@about')->name('about');
Route::get('/contact', 'SiteController@contact')->name('contact');
//Route::group(['middleware'=>'auth.site'], function(){
    Route::get('/commande', 'SiteController@order')->name('order');
    Route::post('/order', 'SiteController@storeOrder')->name('order.store');
//});
Route::get('/services', 'SiteController@services')->name('services');
Route::get('/idees-sites', 'SiteController@ideesSites')->name('idees_sites');
Route::get('/site-login', 'SiteController@login')->name('site.login');
Route::post('/site-register', 'SiteController@register')->name('site.register');
Route::get('/mode-paiement', 'SiteController@paymentMode')->name('mode_paiement');
Route::get('/home', 'SiteController@index')->name('home');
Route::get('/tarif', 'SiteController@tarif')->name('tarif');
Route::post('/contact', 'SiteController@contactUs')->name('contactus');
Route::get('/conditionvente', 'SiteController@sellConditions')->name('condition_vente');
Route::get('/contact-dev', 'SiteController@contactDev')->name('contact_dev');
