<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nom')->nullable();
            $table->string('telephone')->nullable();
            $table->string('code');
            $table->string('type'); //achat || transfert
            $table->tinyInteger('etat'); //initial || en_cours || termine
            $table->string('ville_livraison');
            $table->string('adresse_livraison');
            $table->integer('prix_articles')->nullable();
            $table->integer('commision')->nullable();
            $table->integer('frais_transport_pays')->nullable(); //frais de transport jusqu'au pays
            $table->integer('frais_transport_relais')->nullable(); //frais de transport jusqu'au point relais au cammeroun
            $table->integer('rabais')->nullable();
            $table->timestamps();

            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
