<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*\Illuminate\Support\Facades\DB::table("users")->insert([
            'id' => 1,
            'nom' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role' => 'admin'
        ]);*/
        \Illuminate\Support\Facades\DB::table("users")->insert([
            'nom' => 'mbengshop',
            'email' => 'contact@mbengshop.com',
            'password' => \Illuminate\Support\Facades\Hash::make('contact2020$'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role' => 'admin'
        ]);
        // $this->call(UsersTableSeeder::class);
    }

}
