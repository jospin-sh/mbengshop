$(document).ready(function(){
    var unEuro = 650; //fcfa
    var fraisParKg =6500;
    var pourcentage = 0.1; //10%

    /* Site Order Form */
   $('.article-group').on('click','.btn-add-article', function(e){
       var parentDiv = $(this).closest('.form-group');
       var articleBox = $(this).closest('.article-box');
       var inputClone = articleBox.clone();

       console.log('articleBox input', inputClone);
        /* Resetting clone field */
       inputClone.find('input, number').val('');
       /* Let's remove plus icon to existing article inputs and add minus icon */
       articleBox.find('.btn-add-article').removeClass('btn-add-article')
           .removeClass('icon-plus')
           .addClass('btn-remove-article')
           .addClass('icon-minus');

       /* Now we add a new input for the incoming article */
       parentDiv.find('label').after(inputClone);
   });

    $('.article-group').on('click','.btn-remove-article', function(e){
        $(this).closest('.article-box').remove();
    });

    /* Site TArif form */

    $('#btn-tarif').on('click', function(e){
        e.preventDefault();
        var prix = $('#prix').val() * unEuro;
        var volume = $('#volume').val();

        var total = prix  + volume * fraisParKg + prix * pourcentage;
        $('#tarif-resultat').hide().text(total).fadeIn();
    });

    $('select#service').on('change', function(){
       var service = $(this).val();
       if(service === 'achat'){
           $('input#prix').closest('.form-group').show();
       }else{
           $('input#prix').closest('.form-group').hide();
       }
    });

    /* BO facture  */
    $('.prix_unit, .qte-cmd, .poids').on('input', function(){
        var totalArticle = $(this).closest('tr').find('.prix_total_article');
        var totalArticles = $('.total_article');
        var qte = $(this).closest('tr').find('.qte-cmd').val();
        var prixUnit = $(this).closest('tr').find('.prix_unit').val();
        var prixTotal = qte * prixUnit;
        var prixArticles = 0;
        totalArticle.html(prixTotal);
        $('.prix_total_article').each(function(){
           prixArticles += parseFloat($(this).text());
        });
        totalArticles.html(prixArticles);
        $('#prix_articles').val(prixArticles);

        updateFraisLivraison();
        updateCommission();
        setTotalCommande();

        return false;
    });

    $('#prix_articles, #frais_transport_pays, #commission').on('input', function(){
        setTotalCommande();
    });
    function updateFraisLivraison(){
        var poidsTotal = 0;

        $('.poids').each(function(){
            console.log('poids unit', $(this).val());
           poidsTotal += parseInt($(this).val());
        });

        var fraisLivraison = poidsTotal * fraisParKg;
        console.log('poidsTotal', poidsTotal);
        console.log('fraisLivraison', fraisLivraison);
        $('#frais_transport_pays').val(fraisLivraison);
    }


    function updateCommission(){
        var prixArticles = $('#prix_articles').val();

        var commission = prixArticles * pourcentage;
        $('#commission').val(commission);
    }

    function setTotalCommande(){
        var prixArticles = $('#prix_articles').val();
        var fraisLivraison = $('#frais_transport_pays').val();
        var fraisCommission = $('#commission').val();

        var total = parseInt(prixArticles) + parseInt(fraisLivraison) + parseInt(fraisCommission);
        $('#total_commande').html(total+" FCFA");
    }
});