<?php

namespace App\Http\Controllers;

use App\Article;
use App\Commande;
use App\Constantes;

use App\Contact;
use App\Mail\ContactMail;
use App\Mail\OrderMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    public function index(){
        return view('site/home');
    }

    public function about(){
        return view('site/about');
    }

    public function order(){
        return view('site/order-form');
    }
    public function sellConditions(){
        return view('site/conditions-vente');
    }

    public function storeOrder(Request $request){
        $this->validate($request, [
            'have_image' => 'required',
            'nom' => 'required',
            'telephone' => 'required',
            'email' => 'email',
            'ville' => 'required',
            'adresse' => 'required'
        ]);
        $image = null;
        $lienArticles = null;
        $nomArticles = null;
        if($request->have_image == "no") {
            $this->validate($request, [
                'article' => 'required|array',
                'qte' => 'required|array'
            ]);
            $lienArticles = $request->article;
            $descriptionArticles = $request->description;
            $qteArticles = $request->qte;
        }else  if($request->have_image == "yes"){
            $this->validate($request, [
                'article_with_image' => 'required|array',
                'qte_with_image' => 'required|array',
                'image' => 'required|image'
            ]);

            if($request->hasFile('image')) {
                $image = $request->file('image')
                    ->store('uploads', 'public');
            }
            $nomArticles = $request->article_with_image;
            $descriptionArticles = $request->description_with_image;
            $qteArticles = $request->qte_with_image;
        }else{
            return back()->withInput();
        }
        $userId = (auth()->user()) ? auth()->user()->id : null;
        $commande = Commande::create([
            'code' => uniqid('CMD'),
            'type' => 'achat',
            'etat' => Constantes::ETAT_INITIAL,
            'nom' => $request->nom,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'ville_livraison' => $request->ville,
            'adresse_livraison' => $request->adresse,
            'user_id' => $userId
        ]);

        foreach($qteArticles as $k=>$qte){
            $description = !empty($descriptionArticles[$k]) ? $descriptionArticles[$k] : "";
            $lien = !empty($lienArticles[$k]) ? $lienArticles[$k] : "";
            $nom = !empty($nomArticles[$k]) ? $nomArticles[$k] : "";
            $qte = !empty($qteArticles[$k]) ? $qteArticles[$k] : "";
            Article::create([
                'lien' => $lien,
                'nom' => $nom,
                'description' => $description,
                'quantite' => $qte,
                'poids' => 1,
                'commande_id' => $commande->id,
                'image' => $image
            ]);
        }
//send mail to Rodrigue

        $commande->sujet = "Nouvelle commande sur Mbengshop";
        Mail::to(Constantes::MAIL_ADMIN)->send(new OrderMail($commande));
        Mail::to(Constantes::MAIL_ADMIN2)->send(new OrderMail($commande));

        return redirect('commande')->with('success', 'Votre commande a été envoyée, nous vous contacterons pour la suite de l\'opération');
    }

    public function contact(){
        return view('site/contact');
    }

    public function ideesSites(){
        return view('site/idees-sites');
    }

    public function services(){
        return view('site/services');
    }

    public function login(){
        return view('site/login');
    }

    public function contactUs(Request $request){
        $this->validate($request, [
            'nom' => 'required|max:50',
            'prenom' => 'required|max:50',
            'email' => 'required|max:50',
            'sujet' => 'required',
            'message' => 'required'
        ]);
        $contact = Contact::create([
           'nom' => $request->nom,
           'prenom' => $request->prenom,
           'email' => $request->email,
           'sujet' => $request->sujet,
           'message' => $request->message,
        ]);

        $mailTo = Constantes::MAIL_ADMIN;
        $contact->sujetMail = "Nouveau message sur MBENGSHOP";
        Mail::to($mailTo)->send(new ContactMail($contact));
        Mail::to(Constantes::MAIL_ADMIN2)->send(new ContactMail($contact));

        return redirect('contact')->with('success', 'Votre message a été envoyé avec succès.');
    }

    public function register(Request $request){
        if(empty($request)){
            return view('site/register');
        }

        $this->validate($request, [
            'nom' => 'required|max:50',
            'prenom' => 'required|max:50',
            'telephone' => 'required|max:50',
            'email' =>['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'required|max:50'
        ]);
        User::create([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'telephone' => $request->telephone,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'client'
        ]);
        return redirect('site-login')->with('success', 'Votre compte a été crée avec succès, vous pouvez à présent vous connecter');
    }

    public function paymentMode(){
        return view('site/mode-paiement');
    }

    public function tarif(){
        return view('site/tarif');
    }

    public function contactDev(){
        return view('site/contact-dev');
    }

}
