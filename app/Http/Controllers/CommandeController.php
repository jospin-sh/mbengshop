<?php

namespace App\Http\Controllers;

use App\Article;
use App\Commande;
use App\Constantes;
use App\Mail\BillMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commandes = Commande::orderBy('id', 'desc')->get();
        return view('commande/index', compact('commandes'));
    }

    public function facture(Request $request)
    {
        $commande = Commande::find($request->id);
        return view('commande/facture', compact('commande'));
    }

    public function sendFacture(Request $request)
    {
        $commande = Commande::find($request->id);

        $commande->update([
            "prix_articles" => $request->prix_articles,
            "commission" => $request->commission,
            "etat" => Constantes::ETAT_FACTURE_ENVOYE,
            "frais_transport_pays" => $request->frais_transport_pays,
        ]);

        $articles = $request->articles;
        foreach($articles as $k => $article){
            $articleObj = Article::find($article['id']);

            $nom = !empty($article['nom']) ? $article["nom"] : $articleObj->nom;
            $lien = !empty($article['lien']) ? $article["lien"] : $articleObj->lien;
            $description = !empty($article['description']) ? $article["description"] : $articleObj->description;
            $quantite = !empty($article['quantite']) ? $article["quantite"] : $articleObj->quantite;
            $poids = !empty($article['poids']) ? $article["poids"] : $articleObj->poids;
            $prix = !empty($article['prix']) ? $article["prix"] : $articleObj->prix;
            $articleObj->update([
                'description' => $description,
                'quantite' => $quantite,
                'prix' => $prix,
                'commande_id' => $commande['id']
            ]);
            $articleObj->nom = $nom;
            $articleObj->poids = $poids;
            $articleObj->lien = $lien;
            $articleObj->save();

        }
       // dd($poids);
        $mailTo = $commande->email;
        $commande->sujet = "Votre facture sur Mbengshop";
        Mail::to($mailTo)->send(new BillMail($commande, $articles));

        return redirect()->route('commande', $commande->id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('commande/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $commande = Commande::find($request->id);
        return view('commande/detail', compact('commande'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function edit(Commande $commande)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commande $commande)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        //
    }
}
