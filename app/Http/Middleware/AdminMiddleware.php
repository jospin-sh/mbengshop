<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user() && $request->user()->role != 'admin' && !$request->routeIs('logout'))
        {

            return redirect('home');
			
        }else if ($request->user() && $request->user()->role == 'admin' && $request->routeIs('/home'))
        {
            return redirect('admin/commande');
        }
        return $next($request);
    }
}
