<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $fillable = ['code', 'nom', 'telephone', 'email', 'type', 'etat', 'poids', 'ville_livraison', 'adresse_livraison',
        'prix_articles', 'commission', 'frais_transport_pays', 'frais_transport_relais', 'rabais', 'user_id'];

    public function articles(){
        return $this->hasMany(Article::class);
    }
}
