<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['nom', 'description', 'prix', 'lien', 'image', 'quantite', 'commande_id'];

    public function commande(){
        return $this->belongsTo(Commande::class);
    }
}
