<?php

namespace App\Mail;

use App\Commande;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BillMail extends Mailable
{
    use Queueable, SerializesModels;

    public $commande;
    public $articles;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Commande $commande, array $articles)
    {
        $this->commande = $commande;
        $this->articles = $articles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.bill')->subject($this->commande->sujet)
                ->with(['commande'=>$this->commande, 'articles'=>$this->articles]);
    }
}
